#pragma once

#include <cstdint> // for int32_t
#include <cstdint> // for uint32_t
#include <string>  // for wstring, string
#include <vector>  // for vector

#include "VGMSeq.h" // for ReadMode, VGMSeq
#include "VGMSeqSection.h"

class RawFile;
class VGMSeqSection;

class VGMMultiSectionSeq : public VGMSeq {
public:
  VGMMultiSectionSeq(const std::string &format, RawFile *file, uint32_t offset,
                     uint32_t length = 0, std::wstring name = L"VGM Sequence");
  ~VGMMultiSectionSeq() override;

  void ResetVars() override;
  bool LoadMain() override;

  void AddSection(VGMSeqSection *section);
  bool AddLoopForeverNoItem();
  VGMSeqSection *GetSectionFromOffset(uint32_t offset);

protected:
  bool LoadTracks(ReadMode readMode, int32_t stopTime = 1000000) override;
  virtual bool LoadSection(VGMSeqSection *section, int32_t stopTime = 1000000);
  virtual bool IsOffsetUsed(uint32_t offset);
  virtual bool ReadEvent(int32_t stopTime);

public:
  uint32_t dwStartOffset;
  uint32_t curOffset;

  std::vector<VGMSeqSection *> aSections;

protected:
  int foreverLoops;

private:
  bool GetTrackPointers() override { return false; }
};
