#include "Root.h" // for VGMRoot, pRoot
#include "Scanner.h"

VGMScanner::VGMScanner() = default;

VGMScanner::~VGMScanner() = default;

bool VGMScanner::Init() {
  // if (!UseExtension())
  //	return false;
  return true;
}

void VGMScanner::InitiateScan(RawFile *file, void *offset) {
  pRoot->UI_SetScanInfo();
  this->Scan(file, offset);
}

// void VGMScanner::Scan(RawFile* file)
//{
//	return;
//}
