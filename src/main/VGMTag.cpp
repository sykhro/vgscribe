#include <utility> // for move

#include "VGMTag.h"

VGMTag::VGMTag() = default;

VGMTag::VGMTag(std::wstring _title, std::wstring _artist, std::wstring _album)
    : title(std::move(_title)), album(std::move(_album)),
      artist(std::move(_artist)) {}

VGMTag::~VGMTag() = default;

bool VGMTag::HasTitle() { return !title.empty(); }

bool VGMTag::HasArtist() { return !album.empty(); }

bool VGMTag::HasAlbum() { return !artist.empty(); }

bool VGMTag::HasComment() { return !comment.empty(); }

bool VGMTag::HasTrackNumber() { return track_number != 0; }

bool VGMTag::HasLength() { return length != 0.0; }
