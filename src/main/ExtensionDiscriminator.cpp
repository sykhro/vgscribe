#include <utility> // for pair

#include "ExtensionDiscriminator.h"

// static ExtensionDiscriminator theExtensionDiscriminator;
// ExtensionDiscriminator ExtensionDiscriminator::instance;

ExtensionDiscriminator::ExtensionDiscriminator() = default;

ExtensionDiscriminator::~ExtensionDiscriminator() = default;

int ExtensionDiscriminator::AddExtensionScannerAssoc(std::wstring extension,
                                                     VGMScanner *scanner) {
  mScannerExt[extension].emplace_back(scanner);
  return true;
}

std::list<VGMScanner *> *
ExtensionDiscriminator::GetScannerList(std::wstring extension) {
  auto iter = mScannerExt.find(StringToLower(extension));
  if (iter == mScannerExt.end()) {
    return nullptr;
  }
  { return &(*iter).second; }
}
