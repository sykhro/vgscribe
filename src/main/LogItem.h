#pragma once

#include <ctime>  // for time_t
#include <string> // for wstring

enum LogLevel {
  LOG_LEVEL_ERR,
  LOG_LEVEL_WARN,
  LOG_LEVEL_INFO,
  LOG_LEVEL_DEBUG
};

class LogItem {
public:
  LogItem();
  LogItem(const wchar_t *text, LogLevel level, const wchar_t *source);
  LogItem(std::wstring text, LogLevel level, std::wstring source);
  virtual ~LogItem();

  std::wstring GetText() const;
  const wchar_t *GetCText() const;
  std::time_t GetTime() const;
  LogLevel GetLogLevel() const;
  std::wstring GetSource() const;
  const wchar_t *GetCSource() const;

protected:
  std::wstring text;
  std::wstring source;
  std::time_t time;
  LogLevel level{LOG_LEVEL_ERR};
};
