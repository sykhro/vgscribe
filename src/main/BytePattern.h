// Byte pattern class for flexible byte sequence search
// Heavily inspired by SigScan at GameDeception.net

#ifndef BYTEPATTERN_H
#define BYTEPATTERN_H

#include <cstddef> // for size_t
#include <cstdint>

class BytePattern {
private:
  /** The pattern to scan for */
  char *ptn_str{nullptr};

  /**
   * A mask to ignore certain bytes in the pattern such as addresses.
   * The mask should be as int32_t as all the bytes in the pattern.
   * Use '?' to ignore a byte and 'x' to check it.
   * Example: "xxx????xx" - The first 3 bytes are checked, then the next 4 are
   * ignored, then the last 2 are checked
   */
  char *ptn_mask{nullptr};

  /** The length of ptn_str and ptn_mask (not including a terminating null for
   * ptn_mask) */
  std::size_t ptn_len{0};

public:
  BytePattern();
  BytePattern(const char *pattern, std::size_t length);
  BytePattern(const char *pattern, const char *mask, std::size_t length);
  BytePattern(const BytePattern &obj);
  ~BytePattern();

  bool match(const void *buf, std::size_t buf_len) const;
  bool search(const void *buf, std::size_t buf_len, std::size_t &match_offset,
              std::size_t search_offset = 0) const;
  inline std::size_t length() const { return ptn_len; }
};

#endif
