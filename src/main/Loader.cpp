#include "Loader.h"

class RawFile;

VGMLoader::VGMLoader() = default;

VGMLoader::~VGMLoader() = default;

PostLoadCommand VGMLoader::Apply(RawFile *theFile) { return KEEP_IT; }
