#pragma once
#include <cstdint>                 // for int32_t
#include <cstdint>                 // for uint32_t, uint8_t, uint16_t
#include <cstddef>                 // for size_t
#include <experimental/filesystem> // for path, filesystem
#include <iosfwd>                  // for filebuf, ifstream
#include <string>                  // for wstring
#include <vector>                  // for vector

#include "BytePattern.h"
#include "DataSeg.h" // for DataSeg
#include "VGMTag.h"  // for VGMTag
#include "common.h"

class BytePattern;

namespace fs = std::experimental::filesystem;

class VGMFile;
class VGMItem;

enum ProcessFlag { PF_USELOADERS = 1, PF_USESCANNERS = 2 };

class RawFile {
public:
  RawFile();
  RawFile(fs::path name_, uint32_t file_size = 0, bool bCanRead = true,
          VGMTag tag = VGMTag());
  virtual ~RawFile();

  bool open();
  void close();

  inline std::uint32_t size() { return static_cast<uint32_t>(filesize); }
  inline std::wstring GetFullPath() { return fullpath.wstring(); }
  inline std::wstring GetFileName() { return filename.wstring(); }
  inline std::wstring GetExtension() { return filename.extension().wstring(); }
#ifdef _MSC_VER
  inline const wchar_t *GetCFullPath() { return fullpath.c_str(); }
  inline const wchar_t *GetCFileName() { return filename.c_str(); }
#endif
  inline const std::wstring &GetParRawFileFullPath() {
    return parRawFileFullPath;
  }
  inline std::wstring removeExtFromPath() { return filename.stem().wstring(); }

  VGMItem *GetItemFromOffset(std::int32_t offset);
  VGMFile *GetVGMFileFromOffset(std::int32_t offset);

  virtual int FileRead(void *dest, uint32_t index, uint32_t length);

  void UpdateBuffer(uint32_t index);

  float GetProPreRatio() { return propreRatio; }
  void SetProPreRatio(float newRatio);

  inline uint8_t &operator[](uint32_t offset) {
    if ((offset < buf.startOff) || (offset >= buf.endOff)) {
      UpdateBuffer(offset);
    }
    return buf[offset];
  }

  inline uint8_t GetByte(uint32_t nIndex) {
    if ((nIndex < buf.startOff) || (nIndex + 1 > buf.endOff)) {
      UpdateBuffer(nIndex);
    }
    return buf[nIndex];
  }

  inline uint16_t GetShort(uint32_t nIndex) {
    if ((nIndex < buf.startOff) || (nIndex + 2 > buf.endOff)) {
      UpdateBuffer(nIndex);
    }
    return buf.GetShort(nIndex);
  }

  inline uint32_t GetWord(uint32_t nIndex) {
    if ((nIndex < buf.startOff) || (nIndex + 4 > buf.endOff)) {
      UpdateBuffer(nIndex);
    }
    return buf.GetWord(nIndex);
  }

  inline uint16_t GetShortBE(uint32_t nIndex) {
    if ((nIndex < buf.startOff) || (nIndex + 2 > buf.endOff)) {
      UpdateBuffer(nIndex);
    }
    return buf.GetShortBE(nIndex);
  }

  inline uint32_t GetWordBE(uint32_t nIndex) {
    if ((nIndex < buf.startOff) || (nIndex + 4 > buf.endOff)) {
      UpdateBuffer(nIndex);
    }
    return buf.GetWordBE(nIndex);
  }

  inline bool IsValidOffset(uint32_t nIndex) { return (nIndex < filesize); }

  inline void UseLoaders() { processFlags |= PF_USELOADERS; }
  inline void DontUseLoaders() { processFlags &= ~PF_USELOADERS; }
  inline void UseScanners() { processFlags |= PF_USESCANNERS; }
  inline void DontUseScanners() { processFlags &= ~PF_USESCANNERS; }

  uint32_t GetBytes(uint32_t nIndex, uint32_t nCount, void *pBuffer);
  bool MatchBytes(const uint8_t *pattern, uint32_t nIndex, size_t nCount);
  bool MatchBytePattern(const BytePattern &pattern, uint32_t nIndex);
  bool SearchBytePattern(const BytePattern &pattern, uint32_t &nMatchOffset,
                         uint32_t nSearchOffset = 0,
                         uint32_t nSearchSize = static_cast<uint32_t>(-1));

  void AddContainedVGMFile(VGMFile *vgmfile);
  void RemoveContainedVGMFile(VGMFile *vgmfile);

  bool OnSaveAsRaw();

public:
  DataSeg buf;
  uint32_t bufSize;
  float propreRatio{0.75};
  uint8_t processFlags;

protected:
  std::ifstream file;
  std::filebuf *pbuf;
  bool bCanFileRead{true};
  fs::path fullpath;
  fs::path filename;
  std::uint32_t filesize{0};
  std::wstring parRawFileFullPath;

public:
  std::vector<VGMFile *> containedVGMFiles;
  VGMTag tag;
};

class VirtFile : public RawFile {
public:
  VirtFile();
  VirtFile(uint8_t *data, uint32_t filesize, const std::wstring &name,
           const wchar_t *rawFileName = L"", VGMTag tag = VGMTag());
};
