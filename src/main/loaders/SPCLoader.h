#pragma once
#include "Loader.h" // for PostLoadCommand, VGMLoader

class RawFile;

class SPCLoader : public VGMLoader {
public:
  SPCLoader();

public:
  ~SPCLoader() override;

  PostLoadCommand Apply(RawFile *file) override;
};
