#include "SFLoader.h"

#include "Root.h" // for VGMRoot, pRoot

SFLoader::SFLoader() = default;
SFLoader::~SFLoader() = default;

PostLoadCommand SFLoader::Apply(RawFile *file) {
  uint8_t sig[4];
  file->GetBytes(0, 4, sig);

  if (std::memcmp(sig, SF_BASE_SIGNATURE, 3) == 0) {
    uint8_t version = sig[3];
    uint8_t *exebuf = nullptr;
    size_t exebufsize = NULL;
    std::wstring error;
    switch (version) {
    case VER_PSX:
      exebufsize = PSX_MAX_BUF_SIZE;
      error = read_psf_exe(file, version, exebuf, exebufsize);
      if (!error.empty()) {
        pRoot->AddLogItem(
            new LogItem(std::wstring(error), LOG_LEVEL_ERR, L"PSXF Loader"));
        return KEEP_IT;
      }
      break;

    case VER_PS2:
      break;

    case VER_SNS:
      // TODO(Ely): Fix sections loading
      exebufsize = SNS_MAX_ROM_SIZE;
      error = read_psf_exe(file, version, exebuf, exebufsize);
      if (!error.empty()) {
        pRoot->AddLogItem(
            new LogItem(std::wstring(error), LOG_LEVEL_ERR, L"SNSF Loader"));
        return KEEP_IT;
      }
      break;

    case VER_GSF:
      exebufsize = GSF_MAX_ROM_SIZE;
      error = read_psf_exe(file, version, exebuf, exebufsize);
      if (!error.empty()) {
        pRoot->AddLogItem(
            new LogItem(std::wstring(error), LOG_LEVEL_ERR, L"GSF Loader"));
        return KEEP_IT;
      }
      break;

    case VER_NCS:
    case VER_NDS:
      exebufsize = NDS_MAX_ROM_SIZE;
      error = read_psf_exe(file, version, exebuf, exebufsize);
      if (!error.empty()) {
        pRoot->AddLogItem(new LogItem(std::wstring(error), LOG_LEVEL_ERR,
                                      version == VER_NDS ? L"NDS 2SF Loader"
                                                         : L"NDS ROM Loader"));
        return KEEP_IT;
      }
      break;

    default:
      pRoot->AddLogItem(
          new LogItem(L"Unknown PSF version", LOG_LEVEL_ERR, L"PSF Loader"));
      return KEEP_IT;
    }
    std::wstring str = file->GetFileName();
    pRoot->CreateVirtFile(exebuf, static_cast<uint32_t>(exebufsize), str, L"",
                          file->tag);
    return DELETE_IT;
  }
  return KEEP_IT;
}

std::wstring SFLoader::read_psf_exe(RawFile *file, uint8_t version,
                                    unsigned char *&buffer, size_t &bufsize) {
  PSFFile psf;
  if (!psf.Load(file)) {
    return psf.GetError();
  }

  std::wstring liberror = load_libs(psf, file, version, buffer, bufsize);
  if (!liberror.empty()) {
    return liberror;
  }

  DataSeg *exehead;
  bool exehead_read = false;
  switch (version) {
  case VER_PSX:
    exehead_read = psf.ReadExeDataSeg(exehead, 0x20, 0);
    break;
  case VER_PS2:
    break;
  case VER_SNS:
    exehead_read = psf.ReadExeDataSeg(exehead, 0x08, 0);
    break;
  case VER_GSF:
    exehead_read = psf.ReadExeDataSeg(exehead, 0x0C, 0);
    break;
  case VER_NDS:
    exehead_read = psf.ReadExeDataSeg(exehead, 0x08, 0);
    break;
  case VER_NCS:
    exehead_read = psf.ReadExeDataSeg(exehead, 0x0C, 0);
    break;
  }
  if (!exehead_read) {
    return psf.GetError();
  }

  uint32_t text_start = 0, text_size = 0;
  switch (version) {
  case VER_PSX:
    text_start = exehead->GetWord(0x18) & 0x3FFFFF;
    text_size = exehead->GetWord(0x1C);
    break;
  case VER_PS2:
    break;
  case VER_SNS:
    text_start = exehead->GetWord(0x00);
    text_size = exehead->GetWord(0x04);
    break;
  case VER_GSF:
    text_start = exehead->GetWord(0x04) & 0x01FFFFFF;
    text_size = exehead->GetWord(0x1C);
    if ((exehead->GetWord(0x04) & 0xFE000000) != 0x08000000) {
      return L"Unsupported boot region!";
    }
    break;
  case VER_NDS:
    text_start = exehead->GetWord(0x00);
    text_size = exehead->GetWord(0x04);
    break;
  case VER_NCS:
    text_start = 0;
    text_size = exehead->GetWord(0x08);
    break;
  }

  if (text_start + text_size > bufsize || (buffer == nullptr && bufsize == 0)) {
    return L"ROM start offset and/or size are corrupt";
  }

  if (buffer == nullptr) {
    bufsize = text_start + text_size;
    buffer = new uint8_t[bufsize];
    if (buffer == nullptr) {
      return L"Couldn't allocate buffer!";
    }
    std::memset(buffer, 0, bufsize);
  }

  size_t striplen = 0;
  switch (version) {
  case VER_PSX:
    striplen = 0x800;
    break;
  case VER_PS2:
    break;
  case VER_SNS:
    striplen = 0x08;
    break;
  case VER_GSF:
    striplen = 0x0C;
    break;
  case VER_NDS:
    striplen = 0x08;
    break;
  case VER_NCS:
    striplen = 0x00;
    break;
  }

  if (!psf.ReadExe(buffer + text_start, text_size, striplen)) {
    return L"Decompression failed";
  }

  // FIXME
  if (psf.tags.count("title") != 0) {
    file->tag.title = string2wstring(psf.tags["title"]);
  }
  if (psf.tags.count("artist") != 0) {
    file->tag.artist = string2wstring(psf.tags["artist"]);
  }
  if (psf.tags.count("game") != 0) {
    file->tag.album = string2wstring(psf.tags["game"]);
  }
  if (psf.tags.count("comment") != 0) {
    file->tag.comment = string2wstring(psf.tags["comment"]);
  }

  delete exehead;
  return L"";
}

std::wstring SFLoader::load_libs(PSFFile &psf, RawFile *file, uint8_t version,
                                 unsigned char *&buffer, size_t &bufsize) {
  char libTagName[16];
  int libIndex = 1;
  while (true) {
    if (libIndex == 1) {
      strcpy(libTagName, "_lib");
    } else {
      sprintf(libTagName, "_lib%d", libIndex);
    }

    auto itLibTag = psf.tags.find(libTagName);
    if (itLibTag == psf.tags.end()) {
      break;
    }

    wchar_t tempfn[PATH_MAX] = {0};
    mbstowcs(tempfn, itLibTag->second.c_str(), itLibTag->second.size());

    std::experimental::filesystem::path fullpath = file->GetFullPath();
    fullpath.replace_filename(tempfn);

    // TODO(Ely): Make sure to limit recursion to avoid crashing.
    RawFile *newRawFile = new RawFile(fullpath);
    std::wstring psfLibError;
    if (newRawFile->open()) {
      psfLibError = read_psf_exe(newRawFile, version, buffer, bufsize);
    } else {
      psfLibError = L"Unable to open lib file";
    }
    delete newRawFile;

    if (!psfLibError.empty()) {
      return psfLibError;
    }

    libIndex++;
  }
  return L"";
}
