#include <cstdint> // for uint8_t
#include <cstddef> // for size_t
#include <string>  // for wstring

#include "Loader.h" // for PostLoadCommand, VGMLoader
#include "PSFFile.h"

class PSFFile;
class RawFile;

#define SF_BASE_SIGNATURE "PSF"
#define VER_PSX 0x01
#define VER_PS2 0x02
#define VER_GSF 0x22
#define VER_SNS 0x23
#define VER_NDS 0x24
#define VER_NCS 0x25

#define PSX_MAX_BUF_SIZE 0x200000
#define GSF_MAX_ROM_SIZE 0x2000000
#define SNS_MAX_ROM_SIZE 0x600000
#define NDS_MAX_ROM_SIZE 0x10000000

class SFLoader : public VGMLoader {
public:
  SFLoader();
  ~SFLoader() override;

  PostLoadCommand Apply(RawFile *file) override;
  std::wstring read_psf_exe(RawFile *file, uint8_t version,
                            unsigned char *&buffer, size_t &bufsize);
  std::wstring load_libs(PSFFile &psf, RawFile *file, uint8_t version,
                         unsigned char *&buffer, size_t &bufsize);
};
