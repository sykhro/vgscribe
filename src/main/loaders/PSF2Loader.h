#pragma once
#include <cstdint> // for uint32_t

#include "Loader.h" // for PostLoadCommand, VGMLoader

class RawFile;

#define uint32 unsigned
#define uint16 uint16_t
#define uint8 unsigned char

class PSF2Loader : public VGMLoader {
public:
  PSF2Loader();

public:
  ~PSF2Loader() override;

  PostLoadCommand Apply(RawFile *file) override;
  uint32 get32lsb(const uint8 *src);
  int psf2_decompress_block(RawFile *file, unsigned fileoffset,
                            unsigned blocknumber, unsigned numblocks,
                            unsigned char *decompressedblock,
                            unsigned blocksize);
  int psf2unpack(RawFile *file, uint32_t fileoffset, uint32_t dircount);
};
