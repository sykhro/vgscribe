#pragma once

#include <cstdint> // for uint32_t, uint8_t
#include <string>  // for wstring
#include <vector>  // for vector

#include "SeqTrack.h"
#include "SeqEvent.h"
#include "VGMFile.h"
#include "VGMItem.h" // for EventColors::CLR_HEADER, VGMContainer...
#include "VGMMultiSectionSeq.h"
#include "common.h"

class SeqTrack;
class VGMMultiSectionSeq;

class VGMSeqSection : public VGMContainerItem {
public:
  VGMSeqSection(VGMMultiSectionSeq *parentFile, uint32_t theOffset,
                uint32_t theLength = 0, std::wstring theName = L"Section",
                uint8_t color = CLR_HEADER);
  ~VGMSeqSection() override;

  virtual bool Load();
  virtual bool GetTrackPointers();
  virtual bool PostLoad();

  VGMMultiSectionSeq *parentSeq;
  std::vector<SeqTrack *> aTracks;
};
