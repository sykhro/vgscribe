#include "Root.h" // for VGMRoot, pRoot
#include "VGMMiscFile.h"

// ***********
// VGMMiscFile
// ***********

VGMMiscFile::VGMMiscFile(const std::string &format, RawFile *file, uint32_t offset,
                         uint32_t length, std::wstring name)
    : VGMFile(FILETYPE_MISC, format, file, offset, length, name) {}

bool VGMMiscFile::LoadMain() { return true; }

bool VGMMiscFile::Load() {
  if (!LoadMain()) {
    return false;
  }
  if (unLength == 0) {
    return false;
  }

  LoadLocalData();
  UseLocalData();
  pRoot->AddVGMFile(this);
  return true;
}
