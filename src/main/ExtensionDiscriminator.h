#pragma once

#include <list>   // for list
#include <map>    // for map
#include <string> // for wstring

#include "common.h"

class VGMScanner;

class ExtensionDiscriminator {
public:
  ExtensionDiscriminator();
  ~ExtensionDiscriminator();

  int AddExtensionScannerAssoc(std::wstring extension,
                               VGMScanner * /*scanner*/);
  std::list<VGMScanner *> *GetScannerList(std::wstring extension);

  std::map<std::wstring, std::list<VGMScanner *>> mScannerExt;
  static ExtensionDiscriminator &instance() {
    static ExtensionDiscriminator instance;
    return instance;
  }
};
