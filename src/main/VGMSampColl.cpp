#include <experimental/filesystem> // for path, path::preferred_separator

#include "Root.h"    // for VGMRoot, pRoot
#include "VGMSamp.h" // for VGMSamp
#include "VGMSampColl.h"

class RawFile;

// ***********
// VGMSampColl
// ***********

DECLARE_MENU(VGMSampColl)

VGMSampColl::VGMSampColl(const std::string &format, RawFile *rawfile,
                         uint32_t offset, uint32_t length, std::wstring theName)
    : VGMFile(FILETYPE_SAMPCOLL, format, rawfile, offset, length, theName),
      parInstrSet(nullptr), bLoadOnInstrSetMatch(false), bLoaded(false),
      sampDataOffset(0) {
  AddContainer<VGMSamp>(samples);
}

VGMSampColl::VGMSampColl(const std::string &format, RawFile *rawfile,
                         VGMInstrSet *instrset, uint32_t offset,
                         uint32_t length, std::wstring theName)
    : VGMFile(FILETYPE_SAMPCOLL, format, rawfile, offset, length, theName),
      parInstrSet(instrset), bLoadOnInstrSetMatch(false), bLoaded(false),
      sampDataOffset(0) {
  AddContainer<VGMSamp>(samples);
}

VGMSampColl::~VGMSampColl() { DeleteVect<VGMSamp>(samples); }

bool VGMSampColl::Load() {
  if (bLoaded) {
    return true;
  }
  if (!GetHeaderInfo()) {
    return false;
  }
  if (!GetSampleInfo()) {
    return false;
  }

  if (samples.empty()) {
    return false;
  }

  if (unLength == 0) {
    for (auto samp : samples) {
      // Some formats can have negative sample offset
      // For example, Konami's SNES format and Hudson's SNES format
      // TODO(Ely): Fix negative sample offset without breaking instrument
      // assert(dwOffset <= samp->dwOffset);

      // if (dwOffset > samp->dwOffset)
      //{
      //	unLength += samp->dwOffset - dwOffset;
      //	dwOffset = samp->dwOffset;
      //}

      if (dwOffset + unLength < samp->dwOffset + samp->unLength) {
        unLength = (samp->dwOffset + samp->unLength) - dwOffset;
      }
    }
  }

  UseRawFileData();
  if (!parInstrSet) {
    pRoot->AddVGMFile(this);
  }
  bLoaded = true;
  return true;
}

bool VGMSampColl::GetHeaderInfo() { return true; }

bool VGMSampColl::GetSampleInfo() { return true; }

VGMSamp *VGMSampColl::AddSamp(uint32_t offset, uint32_t length,
                              uint32_t dataOffset, uint32_t dataLength,
                              uint8_t nChannels, uint16_t bps, uint32_t theRate,
                              std::wstring name) {
  samples.emplace_back(new VGMSamp(this, offset, length, dataOffset, dataLength,
                                   nChannels, bps, theRate, name));
  return samples.back();
}

bool VGMSampColl::OnSaveAllAsWav() {
  std::wstring dirpath = pRoot->UI_GetSaveDirPath();
  if (dirpath.length() != 0) {
    for (auto &sample : samples) {
      std::experimental::filesystem::path filepath =
          dirpath +
          std::to_wstring(
              std::experimental::filesystem::path::preferred_separator) +
          ConvertToSafeFileName(sample->name) + L".wav";
      sample->SaveAsWav(filepath);
    }
    return true;
  }
  return false;
}
