#if defined(_MSC_VER) && defined(VGMTRANS_WTL)
#include "stdafx.h"
#include "osdepend.h"
#endif

#if !defined(PCH_H)
#define PCH_H

#include <algorithm>
#include <cassert>
#include <climits>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <cwchar>

#include <cctype>
#include <cstring>
#include <cwchar>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <list>
#include <map>
#include <sstream>
#include <string>
#include <unordered_set>
#include <vector>

#include "portable.h"

// countof definition with fallbacks. Taken from:
// http://www.g-truc.net/post-0708.html
// TODO(Ely): replace this with ibkt the simple C++11 definition when VS2015 is
//  released

// Any compiler claiming C++11 supports, Visual C++ 2015 and Clang version
// supporting constexp
#if __cplusplus >= 201103L || _MSC_VER >= 1900 // C++ 11 implementation
template <typename T, std::size_t N>
constexpr std::size_t countof(T const (&/*unused*/)[N]) noexcept {
  return N;
}

#elif _MSC_VER // Visual C++ fallback
#define countof(arr) _countof(arr)
#else
#define countof(arr) sizeof(arr) / sizeof(arr[0])
#endif

#endif // !defined(PCH_H)