#include <cstdint> // for uint32_t, uint8_t

#include "VGMSeqSection.h" // for VGMSeqSection

VGMSeqSection::VGMSeqSection(VGMMultiSectionSeq *parentFile, uint32_t theOffset,
                             uint32_t theLength, const std::wstring theName,
                             uint8_t color)
    : VGMContainerItem(parentFile, theOffset, theLength, theName, color),
      parentSeq(parentFile) {
  AddContainer<SeqTrack>(aTracks);
}

VGMSeqSection::~VGMSeqSection() { DeleteVect<SeqTrack>(aTracks); }

bool VGMSeqSection::Load() {
  ReadMode readMode = parentSeq->readMode;

  if (readMode == READMODE_ADD_TO_UI) {
    if (!GetTrackPointers()) {
      return false;
    }
  }

  return true;
}

bool VGMSeqSection::GetTrackPointers() { return true; }

bool VGMSeqSection::PostLoad() {
  if (parentSeq->readMode == READMODE_ADD_TO_UI) {
    for (auto &aTrack : aTracks) {
      std::sort(aTrack->aEvents.begin(), aTrack->aEvents.end(),
                [](const VGMItem *a, const VGMItem *b) {
                  return a->dwOffset < b->dwOffset;
                });
    }
  }

  return true;
}
