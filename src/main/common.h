#if !defined(COMMON_H)
#define COMMON_H

#include <cassert>  // for assert
#include <cstdint>  // for uint32_t, uint8_t
#include <stdlib.h> // for mbstowcs, wcstombs, MB_CUR_MAX
#include <iosfwd>   // for istringstream
#include <string>   // for wstring, string

#include "helper.h"
#include "pch.h"

#define VERSION "1.0.3"

#define FORWARD_DECLARE_TYPEDEF_STRUCT(type)                                   \
  struct _##type;                                                              \
  typedef _##type type

/**
Converts a std::string to any class with a proper overload of the >> opertor
@param temp			The string to be converted
@param out	[OUT]	The container for the returned value
*/
template <class T> void FromString(const std::string &temp, T *out) {
  std::istringstream val(temp);
  val >> *out;

  assert(!val.fail());
}

// TODO(Ely): Use fmtcpp for this
uint32_t StringToHex(const std::string &str);
std::wstring StringToLower(std::wstring myString);
std::wstring ConvertToSafeFileName(const std::wstring &str);

inline std::string wstring2string(std::wstring &wstr) {
  auto *mbs = new char[wstr.length() * MB_CUR_MAX + 1];
  wcstombs(mbs, wstr.c_str(), wstr.length() * MB_CUR_MAX + 1);
  std::string str(mbs);
  delete[] mbs;
  return str;
}

inline std::wstring string2wstring(std::string &str) {
  auto *wcs = new wchar_t[str.length() + 1];
  mbstowcs(wcs, str.c_str(), str.length() + 1);
  std::wstring wstr(wcs);
  delete[] wcs;
  return wstr;
}

inline int CountBytesOfVal(const uint8_t *buf, uint32_t numBytes, uint8_t val) {
  int count = 0;
  for (uint32_t i = 0; i < numBytes; i++) {
    if (buf[i] == val) {
      count++;
    }
  }
  return count;
}

struct SizeOffsetPair {
  uint32_t size{0};
  uint32_t offset{0};

  SizeOffsetPair() = default;

  SizeOffsetPair(uint32_t offset, uint32_t size) : size(size), offset(offset) {}
};

wchar_t *GetFileWithBase(const wchar_t *f, const wchar_t *newfile);

#endif // !defined(COMMON_H)
