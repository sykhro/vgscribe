#pragma once

#include <cstdint> // for uint8_t, uint32_t
#include <cstddef> // for size_t
#include <zconf.h> // for Bytef, uLong, uLongf
#include <zlib.h>
#include <cstdint>
#include <map>    // for map
#include <string> // for string, wstring

#include "DataSeg.h"
#include "RawFile.h"

class DataSeg;
class RawFile;

#define PSF_TAG_SIG "[TAG]"
#define PSF_TAG_SIG_LEN 5
#define PSF_STRIP_BUF_SIZE 4096

class PSFFile {
public:
  PSFFile();
  PSFFile(RawFile *file);
  virtual ~PSFFile();

  bool Load(RawFile *file);
  bool ReadExe(uint8_t *buf, size_t len, size_t stripLen) const;
  bool ReadExeDataSeg(DataSeg *&seg, size_t len, size_t stripLen) const;
  bool Decompress(size_t decompressed_size);
  bool IsDecompressed() const;
  uint8_t GetVersion() const;
  size_t GetExeSize() const;
  size_t GetCompressedExeSize() const;
  size_t GetReservedSize() const;
  void Clear();
  inline std::wstring GetError() { return errorstr; }

public:
  PSFFile *parent;
  DataSeg &exe() { return *exeData; }
  DataSeg &reserved() { return *reservedData; }
  std::map<std::string, std::string> tags;

private:
  uint8_t version;
  DataSeg *exeData; // decompressed program section, valid only when it has been
                    // decompressed
  DataSeg *exeCompData;
  DataSeg *reservedData;
  uint32_t exeCRC;
  bool decompressed;
  std::wstring errorstr;
  uint8_t *stripBuf{nullptr};
  size_t stripBufSize{PSF_STRIP_BUF_SIZE};

  int myuncompress(Bytef *dest, uLongf *destLen, const Bytef *source,
                   uLong sourceLen, uLong stripLen) const;
};
