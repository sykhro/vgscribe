#include <utility> // for pair, make_pair

#include "Format.h"
#include "Matcher.h" // for Matcher

FormatMap &Format::registry() {
  static FormatMap registry;
  return registry;
}

Format::Format(const std::string &formatName) : scanner(nullptr), matcher(nullptr) {
  registry().insert(make_pair(formatName, this));
}

Format::~Format() {
  { delete scanner; }
  { delete matcher; }
}

bool Format::Init() {
  scanner = NewScanner();
  matcher = NewMatcher();
  return true;
}

Format *Format::GetFormatFromName(const std::string &name) {
  auto findIt = registry().find(name);
  if (findIt == registry().end()) {
    return nullptr;
  }
  return (*findIt).second;
}

bool Format::OnNewFile(VGMFile *file) {
  if (!matcher) {
    return false;
  }
  return matcher->OnNewFile(file);
}

VGMColl *Format::NewCollection() { return new VGMColl(); }

bool Format::OnCloseFile(VGMFile *file) {
  if (!matcher) {
    return false;
  }
  return matcher->OnCloseFile(file);
}
