#include "LogItem.h"

#include <chrono> // for system_clock

LogItem::LogItem()
    : time(std::chrono::system_clock::to_time_t(
          std::chrono::system_clock::now())) {}

LogItem::LogItem(const wchar_t *text, LogLevel level, const wchar_t *source)
    : text(text ? text : L""), time(std::chrono::system_clock::to_time_t(
                                   std::chrono::system_clock::now())),
      level(level), source(source ? source : L"") {}

LogItem::LogItem(std::wstring text, LogLevel level, std::wstring source)
    : text(std::move(text)), time(std::chrono::system_clock::to_time_t(
                                 std::chrono::system_clock::now())),
      level(level), source(std::move(source)) {}

LogItem::~LogItem() = default;

std::wstring LogItem::GetText() const { return std::wstring(text); }

const wchar_t *LogItem::GetCText() const { return text.c_str(); }

std::time_t LogItem::GetTime() const { return time; }

LogLevel LogItem::GetLogLevel() const { return level; }

std::wstring LogItem::GetSource() const { return std::wstring(source); }

const wchar_t *LogItem::GetCSource() const { return source.c_str(); }
