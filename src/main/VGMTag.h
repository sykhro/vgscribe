#pragma once

#include <cstdint> // for uint8_t
#include <map>     // for map
#include <string>  // for wstring
#include <vector>  // for vector

class VGMTag {
public:
  VGMTag();
  VGMTag(std::wstring _title, std::wstring _artist = L"",
         std::wstring _album = L"");
  virtual ~VGMTag();

  bool HasTitle();
  bool HasArtist();
  bool HasAlbum();
  bool HasComment();
  bool HasTrackNumber();
  bool HasLength();

public:
  std::wstring title;
  std::wstring artist;
  std::wstring album;
  std::wstring comment;
  std::map<std::wstring, std::vector<uint8_t>> binaries;

  /** Track number */
  int track_number{0};

  /** Length in seconds */
  double length{0.0};
};
