#include <cassert> // for assert

#include "RawFile.h"
#include "Root.h"    // for VGMRoot, pRoot
#include "VGMFile.h" // for VGMFile

#define BUF_SIZE 0x100000 // 1mb

RawFile::RawFile()
    : processFlags(PF_USESCANNERS | PF_USELOADERS), parRawFileFullPath(L"") {
  bufSize = (BUF_SIZE > filesize) ? filesize : BUF_SIZE;
}

RawFile::RawFile(fs::path name_, uint32_t file_size, bool bCanRead,
                 const VGMTag tag)
    : fullpath(std::move(name_)), filesize(file_size),
      parRawFileFullPath(L""), // This should only be defined by VirtFile
      propreRatio(0.75), bCanFileRead(bCanRead), tag(tag),
      processFlags(PF_USESCANNERS | PF_USELOADERS) {
  filename = fullpath.filename();
}

RawFile::~RawFile() {
  pRoot->UI_BeginRemoveVGMFiles();

  for (auto x : containedVGMFiles) {
    pRoot->RemoveVGMFile(containedVGMFiles.front(), false);
    containedVGMFiles.erase(containedVGMFiles.begin());
  }

  pRoot->UI_EndRemoveVGMFiles();
  pRoot->UI_CloseRawFile(this);
}

// opens a file using the standard c++ file i/o routines
bool RawFile::open() {
  // Not all the compilers support opening fs::path yet
  file.open(fullpath.string(), std::ios::in | std::ios::binary);

  if (!file.is_open()) {
    pRoot->AddLogItem(new LogItem(
        (std::wstring(L"File ") + filename.wstring() + L" could not be opened"),
        LOG_LEVEL_ERR, L"RawFile"));
    return false;
  }

  // get pointer to associated buffer object
  pbuf = file.rdbuf();

  // get file size using buffer's members
  filesize = static_cast<uint32_t>(fs::file_size(fullpath));

  bufSize = (BUF_SIZE > filesize) ? filesize : BUF_SIZE;
  buf.alloc(bufSize);
  return true;
}

// closes the file
void RawFile::close() { file.close(); }

// Sets the  "ProPre" ratio.  Terrible name, yes.  Every time the buffer is
// updated from an attempt to read at an offset beyond its current bounds, the
// propre ratio determines the new bounds of the buffer.  Literally, it is a
// ratio determining how much of the bounds should be ahead of the current
// offset.  If, for example, the ratio were 0.7, 70% of the buffer would contain
// data ahead of the requested offset and 30% would be below it.
void RawFile::SetProPreRatio(float newRatio) {
  if (newRatio > 1 || newRatio < 0) {
    return;
  }
  propreRatio = newRatio;
}

// given an offset, determines which, if any, VGMItem is encompasses the offset
VGMItem *RawFile::GetItemFromOffset(std::int32_t offset) {
  for (auto &containedVGMFile : containedVGMFiles) {
    VGMItem *item = containedVGMFile->GetItemFromOffset(offset);
    if (item != nullptr) {
      return item;
    }
  }
  return nullptr;
}

// given an offset, determines which, if any, VGMFile encompasses the offset.
VGMFile *RawFile::GetVGMFileFromOffset(std::int32_t offset) {
  for (auto &containedVGMFile : containedVGMFiles) {
    if (containedVGMFile->IsItemAtOffset(offset)) {
      return containedVGMFile;
    }
  }
  return nullptr;
}

// adds the association of a VGMFile to this RawFile
void RawFile::AddContainedVGMFile(VGMFile *vgmfile) {
  containedVGMFiles.emplace_back(vgmfile);
}

// removes the association of a VGMFile with this RawFile
void RawFile::RemoveContainedVGMFile(VGMFile *vgmfile) {
  auto iter = find(containedVGMFiles.begin(), containedVGMFiles.end(), vgmfile);
  if (iter != containedVGMFiles.end()) {
    containedVGMFiles.erase(iter);
  } else {
    pRoot->AddLogItem(
        new LogItem(std::wstring(L"Error: trying to delete a vgmfile which "
                                 L"cannot be found in containedVGMFiles."),
                    LOG_LEVEL_DEBUG, L"RawFile"));
  }

  if (containedVGMFiles.empty()) {
    pRoot->CloseRawFile(this);
  }
}

// read data directly from the file
int RawFile::FileRead(void *dest, uint32_t index, uint32_t length) {
  assert(bCanFileRead);
  assert(index + length <= filesize);
  pbuf->pubseekpos(index, std::ios::in);
  return static_cast<int>(
      pbuf->sgetn(static_cast<char *>(dest),
                  length)); // return bool value based on whether we read all
                            // requested bytes
}

// reads a bunch of data from a given offset.  If the requested data goes beyond
// the bounds of the file buffer, the buffer is updated.  If the requested size
// is greater than the buffer size a direct read from the file is executed.
uint32_t RawFile::GetBytes(uint32_t nIndex, uint32_t nCount, void *pBuffer) {
  if ((nIndex + nCount) > filesize) {
    nCount = filesize - nIndex;
  }

  if (nCount > buf.size) {
    FileRead(pBuffer, nIndex, nCount);
  } else {
    if ((nIndex < buf.startOff) || (nIndex + nCount > buf.endOff)) {
      UpdateBuffer(nIndex);
    }

    memcpy(pBuffer, buf.data + nIndex - buf.startOff, nCount);
  }
  return nCount;
}

// attempts to match the data from a given offset against a given pattern.
// If the requested data goes beyond the bounds of the file buffer, the buffer
// is updated. If the requested size is greater than the buffer size, it always
// fails. (operation not supported)
bool RawFile::MatchBytes(const uint8_t *pattern, uint32_t nIndex,
                         size_t nCount) {
  if ((nIndex + nCount) > filesize) {
    return false;
  }

  if (nCount > buf.size) {
    return false; // not supported
  }
  if ((nIndex < buf.startOff) || (nIndex + nCount > buf.endOff)) {
    UpdateBuffer(nIndex);
  }

  return (memcmp(buf.data + nIndex - buf.startOff, pattern, nCount) == 0);
}

// attempts to match the data from a given offset against a given pattern.
// If the requested data goes beyond the bounds of the file buffer, the buffer
// is updated. If the requested size is greater than the buffer size, it always
// fails. (operation not supported)
bool RawFile::MatchBytePattern(const BytePattern &pattern, uint32_t nIndex) {
  size_t nCount = pattern.length();

  if ((nIndex + nCount) > filesize) {
    return false;
  }

  if (nCount > buf.size) {
    return false; // not supported
  }
  if ((nIndex < buf.startOff) || (nIndex + nCount > buf.endOff)) {
    UpdateBuffer(nIndex);
  }

  return pattern.match(buf.data + nIndex - buf.startOff, nCount);
}

bool RawFile::SearchBytePattern(const BytePattern &pattern,
                                uint32_t &nMatchOffset, uint32_t nSearchOffset,
                                uint32_t nSearchSize) {
  if (nSearchOffset >= filesize) {
    return false;
  }

  if ((nSearchOffset + nSearchSize) > filesize) {
    nSearchSize = filesize - nSearchOffset;
  }

  if (nSearchSize < pattern.length()) {
    return false;
  }

  for (uint32_t nIndex = nSearchOffset;
       nIndex < nSearchOffset + nSearchSize - pattern.length(); nIndex++) {
    if (MatchBytePattern(pattern, nIndex)) {
      nMatchOffset = nIndex;
      return true;
    }
  }
  return false;
}

// Given a requested offset, fills the buffer with data surstd::roundng that
// offset.  The ratio of how much data is read before and after the offset is
// determined by the ProPre ratio (explained above).
void RawFile::UpdateBuffer(uint32_t index) {
  uint32_t beginOffset, endOffset = 0;

  assert(bCanFileRead);

  if (!buf.bAlloced) {
    buf.alloc(bufSize);
  }

  auto proBytes = static_cast<uint32_t>(buf.size * propreRatio);
  uint32_t preBytes = buf.size - proBytes;
  if (proBytes + index > filesize) {
    preBytes += (proBytes + index) - filesize;
    proBytes = filesize - index; // make it go just to the end of the file;
  } else if (preBytes > index) {
    proBytes += preBytes - index;
    preBytes = index;
  }
  beginOffset = index - preBytes;
  endOffset = index + proBytes;

  if (beginOffset >= buf.startOff && beginOffset < buf.endOff) {
    uint32_t prevEndOff = buf.endOff;
    buf.reposition(beginOffset);
    FileRead(buf.data + prevEndOff - buf.startOff, prevEndOff,
             endOffset - prevEndOff);
  } else if (endOffset >= buf.startOff && endOffset < buf.endOff) {
    uint32_t prevStartOff = buf.startOff;
    buf.reposition(beginOffset);
    FileRead(buf.data, beginOffset, prevStartOff - beginOffset);
  } else {
    FileRead(buf.data, beginOffset, buf.size);
    buf.startOff = beginOffset;
    buf.endOff = beginOffset + buf.size;
  }
}

bool RawFile::OnSaveAsRaw() {
  std::wstring filepath =
      pRoot->UI_GetSaveFilePath(ConvertToSafeFileName(filename.wstring()));
  if (filepath.length() != 0) {
    bool result;
    auto *buf = new uint8_t[filesize]; // create a buffer the size of the
                                       // file
    GetBytes(0, filesize, buf);
    result = pRoot->UI_WriteBufferToFile(filepath, buf, filesize);
    delete[] buf;
    return result;
  }
  return false;
}

//  ********
//  VirtFile
//  ********

VirtFile::VirtFile() : RawFile() {}

VirtFile::VirtFile(uint8_t *data, uint32_t filesize, const std::wstring &name,
                   const wchar_t *rawFileName, const VGMTag tag)
    : RawFile(name, filesize, false, tag) {
  parRawFileFullPath = rawFileName;
  buf.load(data, 0, filesize);
}
