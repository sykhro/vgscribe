#pragma once
#include "BytePattern.h"
#include "Scanner.h" // for USE_EXTENSION, VGMScanner

class BytePattern;
class RawFile;

enum MoriSnesVersion : uint8_t; // see MoriSnesFormat.h

class MoriSnesScanner : public VGMScanner {
public:
  MoriSnesScanner() { USE_EXTENSION(L"spc"); }
  ~MoriSnesScanner() override = default;

  void Scan(RawFile *file, void *info = nullptr) override;
  void SearchForMoriSnesFromARAM(RawFile *file);
  void SearchForMoriSnesFromROM(RawFile *file);

private:
  static BytePattern ptnLoadSeq;
  static BytePattern ptnSetDIR;
};
