#pragma once
#include "BytePattern.h"
#include "Scanner.h" // for USE_EXTENSION, VGMScanner

class BytePattern;
class RawFile;

class FalcomSnesScanner : public VGMScanner {
public:
  FalcomSnesScanner() { USE_EXTENSION(L"spc"); }
  ~FalcomSnesScanner() override = default;

  void Scan(RawFile *file, void *info = nullptr) override;
  void SearchForFalcomSnesFromARAM(RawFile *file);
  void SearchForFalcomSnesFromROM(RawFile *file);

private:
  static BytePattern ptnLoadSeq;
  static BytePattern ptnSetDIR;
  static BytePattern ptnLoadInstr;
};
