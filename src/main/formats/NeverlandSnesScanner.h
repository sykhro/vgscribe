#pragma once
#include "BytePattern.h"
#include "Scanner.h" // for USE_EXTENSION, VGMScanner

class BytePattern;
class RawFile;

class NeverlandSnesScanner : public VGMScanner {
public:
  NeverlandSnesScanner() { USE_EXTENSION(L"spc"); }
  ~NeverlandSnesScanner() override = default;

  void Scan(RawFile *file, void *info = nullptr) override;
  void SearchForNeverlandSnesFromARAM(RawFile *file);
  void SearchForNeverlandSnesFromROM(RawFile *file);

private:
  static BytePattern ptnLoadSongSFC;
  static BytePattern ptnLoadSongS2C;
};
