#pragma once
#include <cstdint> // for int32_t, uint32_t, uint16_t, uint8_t
#include <vector>  // for vector

#include "AkaoFormat.h"
#include "Matcher.h"
#include "SeqTrack.h" // for SeqTrack
#include "VGMSeq.h"   // for VGMSeq

class AkaoInstrSet;
class RawFile;

static const uint16_t delta_time_table[] = {0xC0, 0x60, 0x30,   0x18,  0x0C,
                                            0x6,  0x3,  0x20,   0x10,  0x8,
                                            0x4,  0x0,  0xA0A0, 0xA0A0};

class AkaoSeq : public VGMSeq {
public:
  AkaoSeq(RawFile *file, uint32_t offset);
  ~AkaoSeq() override;

  bool GetHeaderInfo() override;
  bool GetTrackPointers() override;

  uint8_t GetNumPositiveBits(uint32_t ulWord);

public:
  AkaoInstrSet *instrset;
  uint16_t seq_id;
  uint16_t assoc_ss_id;
  int nVersion; // AKAO format version number

  bool bUsesIndividualArts;

  enum { VERSION_1, VERSION_2, VERSION_3 };
};

class AkaoTrack : public SeqTrack {
public:
  AkaoTrack(AkaoSeq *parentFile, int32_t offset = 0, int32_t length = 0);

  void ResetVars() override;
  bool ReadEvent() override;

public:
protected:
  uint8_t relative_key, base_key;
  uint32_t current_delta_time;
  uint32_t loop_begin_loc[8];
  uint32_t loopID[8];
  int loop_counter[8];
  int loop_layer;
  int loop_begin_layer;
  bool bNotePlaying;
  std::vector<uint32_t> vCondJumpAddr;
};
