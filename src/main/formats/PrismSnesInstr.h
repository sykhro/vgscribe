#pragma once
#include <cstdint> // for uint16_t, uint32_t, uint8_t
#include <string>  // for wstring
#include <vector>  // for vector

#include "PrismSnesFormat.h" // for PrismSnesVersion
#include "VGMInstrSet.h"     // for VGMInstr, VGMInstrSet
#include "VGMRgn.h"          // for VGMRgn
#include "VGMSampColl.h"

class RawFile;

// *****************
// PrismSnesInstrSet
// *****************

class PrismSnesInstrSet : public VGMInstrSet {
public:
  PrismSnesInstrSet(RawFile *file, PrismSnesVersion ver, uint32_t spcDirAddr,
                    uint16_t addrADSR1Table, uint16_t addrADSR2Table,
                    uint16_t addrTuningTableHigh, uint16_t addrTuningTableLow,
                    const std::wstring &name = L"PrismSnesInstrSet");
  ~PrismSnesInstrSet() override;

  bool GetHeaderInfo() override;
  bool GetInstrPointers() override;

  PrismSnesVersion version;

protected:
  uint32_t spcDirAddr;
  uint16_t addrADSR1Table;
  uint16_t addrADSR2Table;
  uint16_t addrTuningTableHigh;
  uint16_t addrTuningTableLow;
  std::vector<uint8_t> usedSRCNs;
};

// **************
// PrismSnesInstr
// **************

class PrismSnesInstr : public VGMInstr {
public:
  PrismSnesInstr(VGMInstrSet *instrSet, PrismSnesVersion ver, uint8_t srcn,
                 uint32_t spcDirAddr, uint16_t addrADSR1Entry,
                 uint16_t addrADSR2Entry, uint16_t addrTuningEntryHigh,
                 uint16_t addrTuningEntryLow,
                 const std::wstring &name = L"PrismSnesInstr");
  ~PrismSnesInstr() override;

  bool LoadInstr() override;

  PrismSnesVersion version;

protected:
  uint8_t srcn;
  uint32_t spcDirAddr;
  uint16_t addrADSR1Entry;
  uint16_t addrADSR2Entry;
  uint16_t addrTuningEntryHigh;
  uint16_t addrTuningEntryLow;
};

// ************
// PrismSnesRgn
// ************

class PrismSnesRgn : public VGMRgn {
public:
  PrismSnesRgn(PrismSnesInstr *instr, PrismSnesVersion ver, uint8_t srcn,
               uint32_t spcDirAddr, uint16_t addrADSR1Entry,
               uint16_t addrADSR2Entry, uint16_t addrTuningEntryHigh,
               uint16_t addrTuningEntryLow);
  ~PrismSnesRgn() override;

  bool LoadRgn() override;

  PrismSnesVersion version;
};
