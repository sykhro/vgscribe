#include "AkaoFormat.h"

#include "AkaoInstr.h" // for AkaoRgn, AkaoArt, AkaoSampColl, AkaoIn...
#include "AkaoSeq.h"   // for AkaoSeq
#include "PSXSPU.h"    // for PSXConvADSR

class VGMRgn;


bool AkaoColl::LoadMain() {
  auto *instrset = dynamic_cast<AkaoInstrSet *>(instrsets[0]);
  auto *sampcoll = dynamic_cast<AkaoSampColl *>(sampcolls[0]);

  // Set the sample numbers of each region using the articulation data
  // references of each region
  for (auto &aInstr : instrset->aInstrs) {
    auto *instr = dynamic_cast<AkaoInstr *>(aInstr);
    std::vector<VGMRgn *> *rgns = &instr->aRgns;
    for (auto &j : *rgns) {
      auto *rgn = dynamic_cast<AkaoRgn *>(j);

      AkaoArt *art;

      if (!((rgn->artNum - sampcoll->starting_art_id) >= 0 &&
            rgn->artNum - sampcoll->starting_art_id < 200)) {
        pRoot->AddLogItem(
            new LogItem(std::wstring(L"Articualation reference does not exist "
                                     L"in the samp collection"),
                        LOG_LEVEL_ERR, L"AkaoColl"));
        art = &sampcoll->akArts.front();
      }

      if (rgn->artNum - sampcoll->starting_art_id >= sampcoll->akArts.size()) {
        pRoot->AddLogItem(new LogItem(
            std::wstring(L"referencing an articulation that was not loaded"),
            LOG_LEVEL_ERR, L"AkaoColl"));
        art = &sampcoll->akArts.back();
      } else {
        art = &sampcoll->akArts[rgn->artNum - sampcoll->starting_art_id];
      }
      rgn->SetSampNum(art->sample_num);
      if (art->loop_point != 0) {
        rgn->SetLoopInfo(1, art->loop_point,
                         sampcoll->samples[rgn->sampNum]->dataLength -
                             art->loop_point);
      }

      PSXConvADSR<AkaoRgn>(rgn, art->ADSR1, art->ADSR2, false);
      if (instr->bDrumKit) {
        rgn->unityKey = art->unityKey + rgn->keyLow - rgn->drumRelUnityKey;
      } else {
        rgn->unityKey = art->unityKey;
      }

      int16_t ft = art->fineTune;
      if (ft < 0) {
        ft += 0x8000;
      }
      // this gives us the pitch multiplier value ex. 1.05946
      const auto freq_multiplier =
          (((ft * 32) + 0x100000) / static_cast<double>(0x100000));
      double cents = log(freq_multiplier) / log(static_cast<double>(2)) * 1200;
      if (art->fineTune < 0) {
        cents -= 1200;
      }
      rgn->fineTune = static_cast<int16_t>(cents);
    }
  }

  return true;
}

bool AkaoColl::PreDLSMainCreation() {
  // Before DLS Conversion, we want to add instruments for every single
  // articulation definition in the Akao Sample Collection, so that they can be
  // used with the 0xA1 program change sequence event to do this, we create a
  // copy of the AkaoInstrSet before conversion and add the instruments, then
  // when we finish the dls conversion, we put back the original unchanged
  // AkaoInstrSet.  We don't want to make any permanent changes to the InstrSet,
  // in case the collection gets recreated or something.
  /*origInstrSet = (AkaoInstrSet*)instrsets[0];
  AkaoInstrSet* newInstrSet = new AkaoInstrSet(NULL, 0, 0, 0, L"");
  *newInstrSet = *origInstrSet;
  //copy all of the instruments so that we have copied instruments deleted on
  the destructor of the Instrument Set for (uint32_t i=0;
  i<origInstrSet->aInstrs.size(); i++)
  {
      AkaoInstr* cpyInstr = new AkaoInstr(newInstrSet, 0, 0, 0, 0);
      *cpyInstr = *((AkaoInstr*)origInstrSet->aInstrs[i]);
      newInstrSet->aInstrs[i] =  cpyInstr;
      //ditto for all regions in the instruments
      for (uint32_t j=0; j<cpyInstr->aRgns.size(); j++)
      {
          AkaoRgn* cpyRgn = new AkaoRgn(cpyInstr, 0, 0, L"");
          *cpyRgn = *((AkaoRgn*)cpyInstr->aRgns[j]);
          cpyInstr->aRgns[j] =  cpyRgn;
          cpyRgn->items.clear();
      }
  }
  instrsets[0] = newInstrSet;*/

  if (!(dynamic_cast<AkaoSeq *>(seq)
            ->bUsesIndividualArts)) { // only do this if the 0xA1 event
                                      // is actually used
    return true;
  }

  auto *instrSet = dynamic_cast<AkaoInstrSet *>(instrsets[0]);

  auto *sampcoll = dynamic_cast<AkaoSampColl *>(sampcolls[0]);
  const auto numArts = static_cast<const uint32_t>(sampcoll->akArts.size());
  const auto origNumInstrs =
      static_cast<const uint32_t>(instrSet->aInstrs.size());
  if (origNumInstrs + numArts > 0x7F) {
    numAddedInstrs = 0x7F - origNumInstrs;
  } else {
    numAddedInstrs = numArts;
  }

  for (uint32_t i = 0; i < numAddedInstrs; i++) {
    AkaoArt *art = &sampcoll->akArts[i];
    AkaoInstr *newInstr = new AkaoInstr(
        instrSet, 0, 0, 0, origNumInstrs + sampcoll->starting_art_id + i);

    AkaoRgn *rgn = new AkaoRgn(newInstr, 0, 0);

    rgn->SetSampNum(art->sample_num);
    if (art->loop_point != 0) {
      rgn->SetLoopInfo(1, art->loop_point,
                       sampcoll->samples[rgn->sampNum]->dataLength -
                           art->loop_point);
    }

    PSXConvADSR<AkaoRgn>(rgn, art->ADSR1, art->ADSR2, false);
    rgn->unityKey = art->unityKey;

    int16_t ft = art->fineTune;
    if (ft < 0) {
      ft += 0x8000;
    }
    const auto freq_multiplier =
        (((ft * 32) + 0x100000) /
         static_cast<double>(0x100000)); // this gives us the pitch multiplier
                                         // value ex. 1.05946
    double cents = log(freq_multiplier) / log(static_cast<double>(2)) * 1200;
    if (art->fineTune < 0) {
      cents -= 1200;
    }
    rgn->fineTune = static_cast<int16_t>(cents);
    newInstr->aRgns.push_back(rgn);

    instrSet->aInstrs.push_back(newInstr);
  }

  return true;
}

bool AkaoColl::PostDLSMainCreation() {
  // if the 0xA1 event isn't used in the sequence, then we didn't modify the
  // instrset so skip this
  if (!(dynamic_cast<AkaoSeq *>(seq)->bUsesIndividualArts)) {
    return true;
  }

  auto *instrSet = dynamic_cast<AkaoInstrSet *>(instrsets[0]);
  auto *sampcoll = dynamic_cast<AkaoSampColl *>(sampcolls[0]);
  const size_t numArts = sampcoll->akArts.size();
  size_t beginOffset = instrSet->aInstrs.size() - numArts;
  for (size_t i = 0; i < numAddedInstrs; i++) {
    delete instrSet->aInstrs.back();
    instrSet->aInstrs.pop_back();
  }
  return true;
}
