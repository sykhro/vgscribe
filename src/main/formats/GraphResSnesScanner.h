#pragma once
#include <cstdint> // for uint8_t
#include <map>     // for map

#include "BytePattern.h"
#include "Scanner.h" // for USE_EXTENSION, VGMScanner

class BytePattern;
class RawFile;

class GraphResSnesScanner : public VGMScanner {
public:
  GraphResSnesScanner() { USE_EXTENSION(L"spc"); }
  ~GraphResSnesScanner() override = default;

  void Scan(RawFile *file, void *info = nullptr) override;
  void SearchForGraphResSnesFromARAM(RawFile *file);
  void SearchForGraphResSnesFromROM(RawFile *file);

private:
  std::map<uint8_t, uint8_t> GetInitDspRegMap(RawFile *file);

  static BytePattern ptnLoadSeq;
  static BytePattern ptnDspRegInit;
};
