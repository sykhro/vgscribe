#pragma once
#include "Scanner.h" // for VGMScanner

class RawFile;

class OrgScanner : public VGMScanner {
public:
  OrgScanner();
  ~OrgScanner() override;

  void Scan(RawFile *file, void *info = nullptr) override;
  void SearchForOrgSeq(RawFile *file);
};
