#pragma once
#include "BytePattern.h"
#include "Scanner.h" // for USE_EXTENSION, VGMScanner

class BytePattern;
class RawFile;

class SoftCreatSnesScanner : public VGMScanner {
public:
  SoftCreatSnesScanner() { USE_EXTENSION(L"spc"); }
  ~SoftCreatSnesScanner() override = default;

  void Scan(RawFile *file, void *info = nullptr) override;
  void SearchForSoftCreatSnesFromARAM(RawFile *file);
  void SearchForSoftCreatSnesFromROM(RawFile *file);

private:
  static BytePattern ptnLoadSeq;
  static BytePattern ptnVCmdExec;
};
