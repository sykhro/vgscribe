#pragma once
#include "Scanner.h" // for VGMScanner

class RawFile;

class SquarePS2Scanner : public VGMScanner {
public:
  SquarePS2Scanner();

public:
  ~SquarePS2Scanner() override;

  void Scan(RawFile *file, void *info = nullptr) override;
  void SearchForBGMSeq(RawFile *file);
  void SearchForWDSet(RawFile *file);
};
