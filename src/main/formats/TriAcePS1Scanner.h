#pragma once
#include <cstdint> // for uint32_t
#include <vector>  // for vector

#include "Scanner.h" // for VGMScanner

class RawFile;
class TriAcePS1InstrSet;
class TriAcePS1Seq;

class TriAcePS1Scanner : public VGMScanner {
public:
  TriAcePS1Scanner();

public:
  ~TriAcePS1Scanner() override;

  void Scan(RawFile *file, void *info = nullptr) override;
  void SearchForSLZSeq(RawFile *file);
  void SearchForInstrSet(RawFile *file,
                         std::vector<TriAcePS1InstrSet *> &instrsets);
  TriAcePS1Seq *TriAceSLZDecompress(RawFile *file, uint32_t cfOff);
};
