#pragma once
#include <cstdint> // for int16_t, uint32_t, uint8_t, uint16_t
#include <string>  // for wstring
#include <vector>  // for vector

#include "AkaoFormat.h"
#include "Matcher.h"
#include "VGMInstrSet.h" // for VGMInstr, VGMInstrSet
#include "VGMRgn.h"      // for VGMRgn
#include "VGMSampColl.h" // for VGMSampColl

class RawFile;

// ************
// AkaoInstrSet
// ************

class AkaoInstrSet : public VGMInstrSet {
public:
  AkaoInstrSet(RawFile *file, uint32_t length, uint32_t instrOff,
               uint32_t dkitOff, uint32_t theID,
               std::wstring name =
                   L"Akao Instrument Bank" /*, VGMSampColl* sampColl = NULL*/);
  bool GetInstrPointers() override;

public:
  bool bMelInstrs, bDrumKit;
  uint32_t drumkitOff;
};

// *********
// AkaoInstr
// *********

class AkaoInstr : public VGMInstr {
public:
  AkaoInstr(AkaoInstrSet *instrSet, uint32_t offset, uint32_t length,
            uint32_t theBank, uint32_t theInstrNum,
            const std::wstring &name = L"Instrument");
  bool LoadInstr() override;

public:
  uint8_t instrType;
  bool bDrumKit;
};

// ***********
// AkaoDrumKit
// ***********

class AkaoDrumKit : public AkaoInstr {
public:
  AkaoDrumKit(AkaoInstrSet *instrSet, uint32_t offset, uint32_t length,
              uint32_t theBank, uint32_t theInstrNum);
  bool LoadInstr() override;
};

// *******
// AkaoRgn
// *******

class AkaoRgn : public VGMRgn {
public:
  AkaoRgn(VGMInstr *instr, uint32_t offset, uint32_t length = 0,
          const std::wstring &name = L"Region");
  AkaoRgn(VGMInstr *instr, uint32_t offset, uint32_t length, uint8_t keyLow,
          uint8_t keyHigh, uint8_t artIDNum,
          const std::wstring &name = L"Region");

  bool LoadRgn() override;

public:
  uint16_t ADSR1; // raw psx ADSR1 value (articulation data)
  uint16_t ADSR2; // raw psx ADSR2 value (articulation data)
  uint8_t artNum;
  uint8_t drumRelUnityKey;
};

// ***********
// AkaoSampColl
// ***********

typedef struct _AkaoArt {
  uint8_t unityKey;
  int16_t fineTune;
  uint32_t sample_offset;
  uint32_t loop_point;
  uint16_t ADSR1;
  uint16_t ADSR2;
  uint32_t artID;
  int sample_num;
} AkaoArt;

class AkaoSampColl : public VGMSampColl {
public:
  AkaoSampColl(RawFile *file, uint32_t offset, uint32_t length,
               std::wstring name = L"Akao Sample Collection");
  ~AkaoSampColl() override;

  bool GetHeaderInfo() override;
  bool GetSampleInfo() override;

public:
  std::vector<AkaoArt> akArts;
  uint32_t starting_art_id;
  uint16_t sample_set_id;

private:
  uint32_t sample_section_size;
  uint32_t nNumArts;
  uint32_t arts_offset;
  uint32_t sample_section_offset;
};
