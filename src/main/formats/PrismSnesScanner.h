#pragma once
#include "BytePattern.h"
#include "Scanner.h" // for USE_EXTENSION, VGMScanner

class BytePattern;
class RawFile;

class PrismSnesScanner : public VGMScanner {
public:
  PrismSnesScanner() { USE_EXTENSION(L"spc"); }
  ~PrismSnesScanner() override = default;

  void Scan(RawFile *file, void *info = nullptr) override;
  void SearchForPrismSnesFromARAM(RawFile *file);
  void SearchForPrismSnesFromROM(RawFile *file);

private:
  static BytePattern ptnLoadSeq;
  static BytePattern ptnExecVCmd;
  static BytePattern ptnSetDSPd;
  static BytePattern ptnLoadInstr;
  static BytePattern ptnLoadInstrTuning;
};
