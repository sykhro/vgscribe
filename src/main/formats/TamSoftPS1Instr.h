#pragma once
#include <cstdint> // for uint32_t, uint8_t
#include <string>  // for wstring

#include "TamSoftPS1Format.h"
#include "VGMInstrSet.h" // for VGMInstr, VGMInstrSet
#include "VGMRgn.h"      // for VGMRgn
#include "VGMSampColl.h"

class RawFile;

// ******************
// TamSoftPS1InstrSet
// ******************

class TamSoftPS1InstrSet : public VGMInstrSet {
public:
  TamSoftPS1InstrSet(RawFile *file, uint32_t offset, bool ps2,
                     const std::wstring &name = L"TamSoftPS1InstrSet");
  ~TamSoftPS1InstrSet() override;

  bool GetHeaderInfo() override;
  bool GetInstrPointers() override;

  bool ps2;
};

// ***************
// TamSoftPS1Instr
// ***************

class TamSoftPS1Instr : public VGMInstr {
public:
  TamSoftPS1Instr(TamSoftPS1InstrSet *instrSet, uint8_t instrNum,
                  const std::wstring &name = L"TamSoftPS1Instr");
  ~TamSoftPS1Instr() override;

  bool LoadInstr() override;

protected:
  uint32_t spcDirAddr;
  uint32_t addrSampTuningTable;
};

// *************
// TamSoftPS1Rgn
// *************

class TamSoftPS1Rgn : public VGMRgn {
public:
  TamSoftPS1Rgn(TamSoftPS1Instr *instr, uint32_t offset, bool ps2);
  ~TamSoftPS1Rgn() override;

  bool LoadRgn() override;
};
