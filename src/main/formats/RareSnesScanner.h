#pragma once
#include "BytePattern.h"
#include "Scanner.h" // for USE_EXTENSION, VGMScanner

class BytePattern;
class RawFile;

class RareSnesScanner : public VGMScanner {
public:
  RareSnesScanner() { USE_EXTENSION(L"spc"); }
  ~RareSnesScanner() override = default;

  void Scan(RawFile *file, void *info = nullptr) override;
  void SearchForRareSnesFromARAM(RawFile *file);
  void SearchForRareSnesFromROM(RawFile *file);

private:
  static BytePattern ptnLoadDIR;
  static BytePattern ptnReadSRCNTable;
  static BytePattern ptnSongLoadDKC;
  static BytePattern ptnSongLoadDKC2;
  static BytePattern ptnVCmdExecDKC;
  static BytePattern ptnVCmdExecDKC2;
};
