#pragma once
#include "Scanner.h"
class RawFile;

class SonyPS2Scanner : public VGMScanner {
public:
  SonyPS2Scanner() {
    USE_EXTENSION(L"sq")
    USE_EXTENSION(L"hd")
    USE_EXTENSION(L"bd")
  }

public:
  ~SonyPS2Scanner() override = default;

  void Scan(RawFile *file, void *info = nullptr) override;
  void SearchForSeq(RawFile *file);
  void SearchForInstrSet(RawFile *file);
  void SearchForSampColl(RawFile *file);
};
