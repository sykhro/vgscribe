#pragma once
#include <cstdint> // for uint32_t

#include "Scanner.h" // for VGMScanner

class HOSAInstrSet;
class HOSASeq;
class PSXSampColl;
class RawFile;

class HOSAScanner : public VGMScanner {
public:
  HOSAScanner();

public:
  ~HOSAScanner() override;

  void Scan(RawFile *file, void *info = nullptr) override;
  HOSASeq *SearchForHOSASeq(RawFile *file);
  HOSAInstrSet *SearchForHOSAInstrSet(RawFile *file, PSXSampColl *sampcoll);
  bool RecursiveRgnCompare(RawFile *file, int i, int sampNum, int numSamples,
                           int numFinds, uint32_t *sampOffsets);
};
