#pragma once
#include <cstdint>     // for int16_t
#include <cstdint>     // for uint8_t, uint32_t, uint16_t
#include <sys/types.h> // for int8_t
#include <map>         // for map
#include <string>      // for wstring
#include <vector>      // for vector

#include "VGMInstrSet.h" // for VGMInstr, VGMInstrSet
#include "VGMRgn.h"      // for VGMRgn
#include "VGMSampColl.h"

class RawFile;

// ****************
// RareSnesInstrSet
// ****************

class RareSnesInstrSet : public VGMInstrSet {
public:
  RareSnesInstrSet(RawFile *file, uint32_t offset, uint32_t spcDirAddr,
                   const std::wstring &name = L"RareSnesInstrSet");
  RareSnesInstrSet(RawFile *file, uint32_t offset, uint32_t spcDirAddr,
                   std::map<uint8_t, int8_t> instrUnityKeyHints,
                   std::map<uint8_t, int16_t> instrPitchHints,
                   std::map<uint8_t, uint16_t> instrADSRHints,
                   const std::wstring &name = L"RareSnesInstrSet");
  ~RareSnesInstrSet() override;

  virtual void Initialize();
  bool GetHeaderInfo() override;
  bool GetInstrPointers() override;

  const std::vector<uint8_t> &GetAvailableInstruments();

protected:
  uint32_t spcDirAddr;
  uint8_t maxSRCNValue;
  std::vector<uint8_t> availInstruments;
  std::map<uint8_t, int8_t> instrUnityKeyHints;
  std::map<uint8_t, int16_t> instrPitchHints;
  std::map<uint8_t, uint16_t> instrADSRHints;

  void ScanAvailableInstruments();
};

// *************
// RareSnesInstr
// *************

class RareSnesInstr : public VGMInstr {
public:
  RareSnesInstr(VGMInstrSet *instrSet, uint32_t offset, uint32_t theBank,
                uint32_t theInstrNum, uint32_t spcDirAddr, int8_t transpose = 0,
                int16_t pitch = 0, uint16_t adsr = 0x8FE0,
                const std::wstring &name = L"RareSnesInstr");
  ~RareSnesInstr() override;

  bool LoadInstr() override;

protected:
  uint32_t spcDirAddr;
  int8_t transpose;
  int16_t pitch;
  uint16_t adsr;
};

// ***********
// RareSnesRgn
// ***********

class RareSnesRgn : public VGMRgn {
public:
  RareSnesRgn(RareSnesInstr *instr, uint32_t offset, int8_t transpose = 0,
              int16_t pitch = 0, uint16_t adsr = 0x8FE0);
  ~RareSnesRgn() override;

  bool LoadRgn() override;

protected:
  int8_t transpose;
  int16_t pitch;
  uint16_t adsr;
};
