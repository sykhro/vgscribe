#pragma once
#include <cstdint>     // for uint16_t, uint32_t, uint8_t
#include <sys/types.h> // for int8_t
#include <map>         // for map
#include <string>      // for wstring
#include <vector>      // for vector

#include "MoriSnesFormat.h" // for MoriSnesVersion
#include "VGMInstrSet.h"    // for VGMInstr, VGMInstrSet
#include "VGMRgn.h"         // for VGMRgn
#include "VGMSampColl.h"

class RawFile;

struct MoriSnesInstrHint {
  MoriSnesInstrHint() = default;

  uint16_t startAddress{0};
  uint16_t size{0};
  uint16_t seqAddress{0};
  uint16_t seqSize{0};
  uint16_t rgnAddress{0};
  int8_t transpose{0};
  int8_t pan{0};
};

struct MoriSnesInstrHintDir {
  MoriSnesInstrHintDir() = default;

  uint16_t startAddress{0};
  uint16_t size{0};

  bool percussion{false};
  MoriSnesInstrHint instrHint;
  std::vector<MoriSnesInstrHint> percHints;
};

// ****************
// MoriSnesInstrSet
// ****************

class MoriSnesInstrSet : public VGMInstrSet {
public:
  MoriSnesInstrSet(RawFile *file, MoriSnesVersion ver, uint32_t spcDirAddr,
                   std::vector<uint16_t> instrumentAddresses,
                   std::map<uint16_t, MoriSnesInstrHintDir> instrumentHints,
                   const std::wstring &name = L"MoriSnesInstrSet");
  ~MoriSnesInstrSet() override;

  bool GetHeaderInfo() override;
  bool GetInstrPointers() override;

  MoriSnesVersion version;

protected:
  uint32_t spcDirAddr;
  std::vector<uint16_t> instrumentAddresses;
  std::map<uint16_t, MoriSnesInstrHintDir> instrumentHints;
  std::vector<uint8_t> usedSRCNs;
};

// *************
// MoriSnesInstr
// *************

class MoriSnesInstr : public VGMInstr {
public:
  MoriSnesInstr(VGMInstrSet *instrSet, MoriSnesVersion ver, uint8_t instrNum,
                uint32_t spcDirAddr, const MoriSnesInstrHintDir &instrHintDir,
                const std::wstring &name = L"MoriSnesInstr");
  ~MoriSnesInstr() override;

  bool LoadInstr() override;

  MoriSnesVersion version;

protected:
  uint32_t spcDirAddr;
  MoriSnesInstrHintDir instrHintDir;
};

// ***********
// MoriSnesRgn
// ***********

class MoriSnesRgn : public VGMRgn {
public:
  MoriSnesRgn(MoriSnesInstr *instr, MoriSnesVersion ver, uint32_t spcDirAddr,
              const MoriSnesInstrHint &instrHint, int8_t percNoteKey = -1);
  ~MoriSnesRgn() override;

  bool LoadRgn() override;

  MoriSnesVersion version;
};
