#include <cstddef> // for size_t

#include "NeverlandSnesSeq.h"

class RawFile;

DECLARE_FORMAT(NeverlandSnes);

//  ****************
//  NeverlandSnesSeq
//  ****************
#define MAX_TRACKS 8
#define SEQ_PPQN 48

NeverlandSnesSeq::NeverlandSnesSeq(RawFile *file, NeverlandSnesVersion ver,
                                   uint32_t seqdataOffset)
    : VGMSeq(NeverlandSnesFormat::name, file, seqdataOffset), version(ver) {
  bLoadTickByTick = true;
  bAllowDiscontinuousTrackData = true;
  bUseLinearAmplitudeScale = true;

  UseReverb();
  AlwaysWriteInitialReverb(0);

  LoadEventMap();
}

NeverlandSnesSeq::~NeverlandSnesSeq() = default;

void NeverlandSnesSeq::ResetVars() { VGMSeq::ResetVars(); }

bool NeverlandSnesSeq::GetHeaderInfo() {
  SetPPQN(SEQ_PPQN);

  VGMHeader *header = AddHeader(dwOffset, 0);
  if (version == NEVERLANDSNES_SFC) {
    header->unLength = 0x40;
  } else if (version == NEVERLANDSNES_S2C) {
    header->unLength = 0x50;
  }

  if (dwOffset + header->unLength >= 0x10000) {
    return false;
  }

  header->AddSimpleItem(dwOffset, 3, L"Signature");
  header->AddUnknownItem(dwOffset + 3, 1);

  const size_t NAME_SIZE = 12;
  char rawName[NAME_SIZE + 1] = {0};
  GetBytes(dwOffset + 4, NAME_SIZE, rawName);
  header->AddSimpleItem(dwOffset + 4, 12, L"Song Name");

  // trim name text
  for (int i = NAME_SIZE - 1; i >= 0; i--) {
    if (rawName[i] != ' ') {
      break;
    }
    rawName[i] = '\0';
  }
  // set name to the sequence
  if (rawName[0] != ('\0')) {
    std::string nameStr = std::string(rawName);
    name = string2wstring(nameStr);
  } else {
    name = L"NeverlandSnesSeq";
  }

  for (uint8_t trackIndex = 0; trackIndex < MAX_TRACKS; trackIndex++) {
    uint16_t trackSignPtr = dwOffset + 0x10 + trackIndex;
    uint8_t trackSign = GetByte(trackSignPtr);

    std::wstringstream trackSignName;
    trackSignName << L"Track " << (trackIndex + 1) << L" Entry";
    header->AddSimpleItem(trackSignPtr, 1, trackSignName.str());

    uint16_t sectionListOffsetPtr = dwOffset + 0x20 + (trackIndex * 2);
    if (trackSign != 0xff) {
      uint16_t sectionListAddress = GetShortAddress(sectionListOffsetPtr);

      std::wstringstream playlistName;
      playlistName << L"Track " << (trackIndex + 1) << L" Playlist Pointer";
      header->AddSimpleItem(sectionListOffsetPtr, 2, playlistName.str());

      aTracks.emplace_back(new NeverlandSnesTrack(this, sectionListAddress));
    } else {
      header->AddSimpleItem(sectionListOffsetPtr, 2, L"NULL");
    }
  }

  return true;
}

bool NeverlandSnesSeq::GetTrackPointers() { return true; }

void NeverlandSnesSeq::LoadEventMap() {
  // TODO(Ely): NeverlandSnesSeq::LoadEventMap
}

uint16_t NeverlandSnesSeq::ConvertToAPUAddress(uint16_t offset) {
  if (version == NEVERLANDSNES_S2C) {
    return dwOffset + offset;
  }
  return offset;
}

uint16_t NeverlandSnesSeq::GetShortAddress(uint32_t offset) {
  return ConvertToAPUAddress(GetShort(offset));
}

//  ******************
//  NeverlandSnesTrack
//  ******************

NeverlandSnesTrack::NeverlandSnesTrack(NeverlandSnesSeq *parentFile,
                                       int32_t offset, int32_t length)
    : SeqTrack(parentFile, offset, length) {
  ResetVars();
  bDetermineTrackLengthEventByEvent = true;
  bWriteGenericEventAsTextEvent = false;
}

void NeverlandSnesTrack::ResetVars() { SeqTrack::ResetVars(); }

bool NeverlandSnesTrack::ReadEvent() {
  auto *parentSeq = dynamic_cast<NeverlandSnesSeq *>(this->parentSeq);

  uint32_t beginOffset = curOffset;
  if (curOffset >= 0x10000) {
    return false;
  }

  uint8_t statusByte = GetByte(curOffset++);
  bool bContinue = true;

  std::wstringstream desc;

  auto eventType = static_cast<NeverlandSnesSeqEventType>(0);
  auto pEventType = parentSeq->EventMap.find(statusByte);
  if (pEventType != parentSeq->EventMap.end()) {
    eventType = pEventType->second;
  }

  switch (eventType) {
  case EVENT_UNKNOWN0:
    desc << L"Event: 0x" << std::hex << std::setfill(L'0') << std::setw(2)
         << std::uppercase << static_cast<int>(statusByte);
    AddUnknown(beginOffset, curOffset - beginOffset, L"Unknown Event",
               desc.str());
    break;

  case EVENT_UNKNOWN1: {
    uint8_t arg1 = GetByte(curOffset++);
    desc << L"Event: 0x" << std::hex << std::setfill(L'0') << std::setw(2)
         << std::uppercase << static_cast<int>(statusByte) << std::dec
         << std::setfill(L' ') << std::setw(0) << L"  Arg1: "
         << static_cast<int>(arg1);
    AddUnknown(beginOffset, curOffset - beginOffset, L"Unknown Event",
               desc.str());
    break;
  }

  case EVENT_UNKNOWN2: {
    uint8_t arg1 = GetByte(curOffset++);
    uint8_t arg2 = GetByte(curOffset++);
    desc << L"Event: 0x" << std::hex << std::setfill(L'0') << std::setw(2)
         << std::uppercase << static_cast<int>(statusByte) << std::dec
         << std::setfill(L' ') << std::setw(0) << L"  Arg1: "
         << static_cast<int>(arg1) << L"  Arg2: " << static_cast<int>(arg2);
    AddUnknown(beginOffset, curOffset - beginOffset, L"Unknown Event",
               desc.str());
    break;
  }

  case EVENT_UNKNOWN3: {
    uint8_t arg1 = GetByte(curOffset++);
    uint8_t arg2 = GetByte(curOffset++);
    uint8_t arg3 = GetByte(curOffset++);
    desc << L"Event: 0x" << std::hex << std::setfill(L'0') << std::setw(2)
         << std::uppercase << static_cast<int>(statusByte) << std::dec
         << std::setfill(L' ') << std::setw(0) << L"  Arg1: "
         << static_cast<int>(arg1) << L"  Arg2: " << static_cast<int>(arg2)
         << L"  Arg3: " << static_cast<int>(arg3);
    AddUnknown(beginOffset, curOffset - beginOffset, L"Unknown Event",
               desc.str());
    break;
  }

  case EVENT_UNKNOWN4: {
    uint8_t arg1 = GetByte(curOffset++);
    uint8_t arg2 = GetByte(curOffset++);
    uint8_t arg3 = GetByte(curOffset++);
    uint8_t arg4 = GetByte(curOffset++);
    desc << L"Event: 0x" << std::hex << std::setfill(L'0') << std::setw(2)
         << std::uppercase << static_cast<int>(statusByte) << std::dec
         << std::setfill(L' ') << std::setw(0) << L"  Arg1: "
         << static_cast<int>(arg1) << L"  Arg2: " << static_cast<int>(arg2)
         << L"  Arg3: " << static_cast<int>(arg3) << L"  Arg4: "
         << static_cast<int>(arg4);
    AddUnknown(beginOffset, curOffset - beginOffset, L"Unknown Event",
               desc.str());
    break;
  }

  default:
    desc << L"Event: 0x" << std::hex << std::setfill(L'0') << std::setw(2)
         << std::uppercase << static_cast<int>(statusByte);
    AddUnknown(beginOffset, curOffset - beginOffset, L"Unknown Event",
               desc.str());
    pRoot->AddLogItem(
        new LogItem(std::wstring(L"Unknown Event - ") + desc.str(),
                    LOG_LEVEL_ERR, std::wstring(L"NeverlandSnesSeq")));
    bContinue = false;
    break;
  }

  // std::wostringstream ssTrace;
  // ssTrace << L"" << std::hex << std::setfill(L'0') << std::setw(8) <<
  // std::uppercase << beginOffset << L": " << std::setw(2) << (int)statusByte
  // << L" -> " << std::setw(8) << curOffset << '\n';
  // OutputDebugString(ssTrace.str().c_str());

  return bContinue;
}

uint16_t NeverlandSnesTrack::ConvertToAPUAddress(uint16_t offset) {
  auto *parentSeq = dynamic_cast<NeverlandSnesSeq *>(this->parentSeq);
  return parentSeq->ConvertToAPUAddress(offset);
}

uint16_t NeverlandSnesTrack::GetShortAddress(uint32_t offset) {
  auto *parentSeq = dynamic_cast<NeverlandSnesSeq *>(this->parentSeq);
  return parentSeq->GetShortAddress(offset);
}
