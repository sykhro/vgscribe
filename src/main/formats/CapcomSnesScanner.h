#pragma once
#include <cstdint>     // for uint16_t, uint8_t, uint32_t
#include <sys/types.h> // for int8_t
#include <map>         // for map

#include "BytePattern.h"
#include "Scanner.h" // for USE_EXTENSION, VGMScanner

class BytePattern;
class RawFile;

enum CapcomSnesVersion : uint8_t; // see CapcomSnesFormat.h

class CapcomSnesScanner : public VGMScanner {
public:
  CapcomSnesScanner() { USE_EXTENSION(L"spc"); }
  ~CapcomSnesScanner() override = default;

  void Scan(RawFile *file, void *info = nullptr) override;
  void SearchForCapcomSnesFromARAM(RawFile *file);
  void SearchForCapcomSnesFromROM(RawFile *file);

private:
  int GetLengthOfSongList(RawFile *file, uint16_t addrSongList);
  uint16_t GetCurrentPlayAddressFromARAM(RawFile *file,
                                         CapcomSnesVersion version,
                                         uint8_t channel);
  int8_t GuessCurrentSongFromARAM(RawFile *file, CapcomSnesVersion version,
                                  uint16_t addrSongList);
  bool IsValidBGMHeader(RawFile *file, uint32_t addrSongHeader);
  std::map<uint8_t, uint8_t> GetInitDspRegMap(RawFile *file);

  static BytePattern ptnReadSongList;
  static BytePattern ptnReadBGMAddress;
  static BytePattern ptnDspRegInit;
  static BytePattern ptnDspRegInitOldVer;
  static BytePattern ptnLoadInstrTableAddress;
};
