#pragma once
#include <cstdint> // for uint16_t, uint32_t, uint8_t
#include <string>  // for wstring
#include <vector>  // for vector

#include "ChunSnesFormat.h" // for ChunSnesVersion
#include "VGMInstrSet.h"    // for VGMInstr, VGMInstrSet
#include "VGMRgn.h"         // for VGMRgn
#include "VGMSampColl.h"

class RawFile;

// ****************
// ChunSnesInstrSet
// ****************

class ChunSnesInstrSet : public VGMInstrSet {
public:
  ChunSnesInstrSet(RawFile *file, ChunSnesVersion ver, uint16_t addrInstrSet,
                   uint16_t addrSampNumTable, uint16_t addrSampleTable,
                   uint32_t spcDirAddr,
                   const std::wstring &name = L"ChunSnesInstrSet");
  ~ChunSnesInstrSet() override;

  bool GetHeaderInfo() override;
  bool GetInstrPointers() override;

  ChunSnesVersion version;

protected:
  uint32_t spcDirAddr;
  uint16_t addrSampNumTable;
  uint16_t addrSampleTable;
  std::vector<uint8_t> usedSRCNs;
};

// *************
// ChunSnesInstr
// *************

class ChunSnesInstr : public VGMInstr {
public:
  ChunSnesInstr(VGMInstrSet *instrSet, ChunSnesVersion ver, uint8_t theInstrNum,
                uint16_t addrInstr, uint16_t addrSampleTable,
                uint32_t spcDirAddr,
                const std::wstring &name = L"ChunSnesInstr");
  ~ChunSnesInstr() override;

  bool LoadInstr() override;

  ChunSnesVersion version;

protected:
  uint16_t addrSampleTable;
  uint32_t spcDirAddr;
};

// ***********
// ChunSnesRgn
// ***********

class ChunSnesRgn : public VGMRgn {
public:
  ChunSnesRgn(ChunSnesInstr *instr, ChunSnesVersion ver, uint8_t srcn,
              uint16_t addrRgn, uint32_t spcDirAddr);
  ~ChunSnesRgn() override;

  bool LoadRgn() override;

  ChunSnesVersion version;
};
