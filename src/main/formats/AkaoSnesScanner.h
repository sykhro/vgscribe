#pragma once
#include "BytePattern.h"
#include "Scanner.h" // for USE_EXTENSION, VGMScanner

class BytePattern;
class RawFile;

enum AkaoSnesVersion : uint8_t; // see AkaoSnesFormat.h

class AkaoSnesScanner : public VGMScanner {
public:
  AkaoSnesScanner() { USE_EXTENSION(L"spc"); }
  ~AkaoSnesScanner() override = default;

  void Scan(RawFile *file, void *info = nullptr) override;
  void SearchForAkaoSnesFromARAM(RawFile *file);
  void SearchForAkaoSnesFromROM(RawFile *file);

private:
  static BytePattern ptnReadNoteLengthV1;
  static BytePattern ptnReadNoteLengthV2;
  static BytePattern ptnReadNoteLengthV4;
  static BytePattern ptnVCmdExecFF4;
  static BytePattern ptnVCmdExecRS3;
  static BytePattern ptnReadSeqHeaderV1;
  static BytePattern ptnReadSeqHeaderV2;
  static BytePattern ptnReadSeqHeaderFFMQ;
  static BytePattern ptnReadSeqHeaderV4;
  static BytePattern ptnLoadDIRV1;
  static BytePattern ptnLoadDIRV3;
  static BytePattern ptnLoadInstrV1;
  static BytePattern ptnLoadInstrV2;
  static BytePattern ptnLoadInstrV3;
  static BytePattern ptnReadPercussionTableV4;
};
