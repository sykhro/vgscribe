#pragma once
#include <vector> // for vector

#include "HeartBeatPS1Seq.h"
#include "Scanner.h" // for VGMScanner
#include "Vab.h"

class RawFile;
class VGMFile;

class HeartBeatPS1Scanner : public VGMScanner {
public:
  HeartBeatPS1Scanner();
  ~HeartBeatPS1Scanner() override;

  void Scan(RawFile *file, void *info = nullptr) override;
  std::vector<VGMFile *> SearchForHeartBeatPS1VGMFile(RawFile *file);
};
