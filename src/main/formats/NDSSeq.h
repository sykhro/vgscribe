#pragma once
#include <cstdint> // for uint32_t, uint8_t
#include <string>  // for wstring

#include "NDSFormat.h"
#include "SeqTrack.h" // for SeqTrack
#include "VGMSeq.h"   // for VGMSeq

class RawFile;

class NDSSeq : public VGMSeq {
public:
  NDSSeq(RawFile *file, uint32_t offset, uint32_t length = 0,
         std::wstring name = L"NDSSeq");

  bool GetHeaderInfo() override;
  bool GetTrackPointers() override;
};

class NDSTrack : public SeqTrack {
public:
  NDSTrack(NDSSeq *parentFile, uint32_t offset = 0, uint32_t length = 0);
  void ResetVars() override;
  bool ReadEvent() override;

  uint8_t jumpCount;
  uint32_t loopReturnOffset;
  bool hasLoopReturnOffset;
  bool noteWithDelta;
};

/*
 * Track commands
 */

#define SEQ_MULTITRACK 0xfe

#define SEQ_CMD_WAIT 0x80
#define SEQ_CMD_PRG 0x81

#define SEQ_CMD_OPEN_TRACK 0x93
#define SEQ_CMD_JUMP 0x94
#define SEQ_CMD_CALL 0x95

#define SEQ_CMD_RANDOM 0xa0
#define SEQ_CMD_VARIABLE 0xa1
#define SEQ_CMD_IF 0xa2

#define SEQ_CMD_SETVAR 0xb0
#define SEQ_CMD_ADDVAR 0xb1
#define SEQ_CMD_SUBVAR 0xb2
#define SEQ_CMD_MULVAR 0xb3
#define SEQ_CMD_DIVVAR 0xb4
#define SEQ_CMD_SHIFTVAR 0xb5
#define SEQ_CMD_RANDVAR 0xb6

#define SEQ_CMD_CMP_EQ 0xb8
#define SEQ_CMD_CMP_GE 0xb9
#define SEQ_CMD_CMP_GT 0xba
#define SEQ_CMD_CMP_LE 0xbb
#define SEQ_CMD_CMP_LT 0xbc
#define SEQ_CMD_CMP_NE 0xbd

#define SEQ_CMD_PAN 0xc0
#define SEQ_CMD_VOLUME 0xc1
#define SEQ_CMD_MAIN_VOLUME 0xc2
#define SEQ_CMD_TRANSPOSE 0xc3
#define SEQ_CMD_PITCH_BEND 0xc4
#define SEQ_CMD_BEND_RANGE 0xc5
#define SEQ_CMD_PRIO 0xc6
#define SEQ_CMD_NOTE_WAIT 0xc7
#define SEQ_CMD_TIE 0xc8
#define SEQ_CMD_PORTA 0xc9
#define SEQ_CMD_MOD_DEPTH 0xca
#define SEQ_CMD_MOD_SPEED 0xcb
#define SEQ_CMD_MOD_TYPE 0xcc
#define SEQ_CMD_MOD_RANGE 0xcd
#define SEQ_CMD_PORTA_SW 0xce
#define SEQ_CMD_PORTA_TIME 0xcf
#define SEQ_CMD_ATTACK 0xd0
#define SEQ_CMD_DECAY 0xd1
#define SEQ_CMD_SUSTAIN 0xd2
#define SEQ_CMD_RELEASE 0xd3
#define SEQ_CMD_LOOP_START 0xd4
#define SEQ_CMD_VOLUME2 0xd5
#define SEQ_CMD_PRINTVAR 0xd6
#define SEQ_CMD_MUTE 0xd7

#define SEQ_CMD_MOD_DELAY 0xe0
#define SEQ_CMD_TEMPO 0xe1
#define SEQ_CMD_SWEEP_PITCH 0xe3

#define SEQ_CMD_LOOP_END 0xfc
#define SEQ_CMD_RET 0xfd
#define SEQ_CMD_ALLOC_TRACK 0xfe
#define SEQ_CMD_FIN 0xff
