#pragma once
#include <cstdint> // for uint32_t, uint8_t
#include <string>  // for wstring
#include <vector>  // for vector

#include "HudsonSnesFormat.h" // for HudsonSnesVersion
#include "VGMInstrSet.h"      // for VGMInstr, VGMInstrSet
#include "VGMRgn.h"           // for VGMRgn
#include "VGMSampColl.h"

class RawFile;

// ******************
// HudsonSnesInstrSet
// ******************

class HudsonSnesInstrSet : public VGMInstrSet {
public:
  HudsonSnesInstrSet(RawFile *file, HudsonSnesVersion ver, uint32_t offset,
                     uint32_t length, uint32_t spcDirAddr,
                     uint32_t addrSampTuningTable,
                     const std::wstring &name = L"HudsonSnesInstrSet");
  ~HudsonSnesInstrSet() override;

  bool GetHeaderInfo() override;
  bool GetInstrPointers() override;

  HudsonSnesVersion version;

protected:
  uint32_t spcDirAddr;
  uint32_t addrSampTuningTable;
  std::vector<uint8_t> usedSRCNs;
};

// ***************
// HudsonSnesInstr
// ***************

class HudsonSnesInstr : public VGMInstr {
public:
  HudsonSnesInstr(VGMInstrSet *instrSet, HudsonSnesVersion ver, uint32_t offset,
                  uint8_t instrNum, uint32_t spcDirAddr,
                  uint32_t addrSampTuningTable,
                  const std::wstring &name = L"HudsonSnesInstr");
  ~HudsonSnesInstr() override;

  bool LoadInstr() override;

  HudsonSnesVersion version;

protected:
  uint32_t spcDirAddr;
  uint32_t addrSampTuningTable;
};

// *************
// HudsonSnesRgn
// *************

class HudsonSnesRgn : public VGMRgn {
public:
  HudsonSnesRgn(HudsonSnesInstr *instr, HudsonSnesVersion ver, uint32_t offset,
                uint32_t spcDirAddr, uint32_t addrTuningEntry);
  ~HudsonSnesRgn() override;

  bool LoadRgn() override;

  HudsonSnesVersion version;
};
