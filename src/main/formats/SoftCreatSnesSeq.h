#pragma once
#include <cstdint> // for int32_t
#include <cstdint> // for uint8_t, uint32_t
#include <map>     // for map
#include <string>  // for wstring

#include "SeqEvent.h"
#include "SeqTrack.h"            // for SeqTrack
#include "SoftCreatSnesFormat.h" // for SoftCreatSnesVersion
#include "VGMSeq.h"              // for VGMSeq

class RawFile;

enum SoftCreatSnesSeqEventType {
  // start enum at 1 because if map[] look up fails, it returns 0, and we don't
  // want that to get confused with a legit event
  EVENT_UNKNOWN0 = 1,
  EVENT_UNKNOWN1,
  EVENT_UNKNOWN2,
  EVENT_UNKNOWN3,
  EVENT_UNKNOWN4,
};

class SoftCreatSnesSeq : public VGMSeq {
public:
  SoftCreatSnesSeq(RawFile *file, SoftCreatSnesVersion ver,
                   uint32_t seqdataOffset, uint8_t headerAlignSize,
                   std::wstring newName = L"SoftCreat SNES Seq");
  ~SoftCreatSnesSeq() override;

  bool GetHeaderInfo() override;
  bool GetTrackPointers() override;
  void ResetVars() override;

  SoftCreatSnesVersion version;
  std::map<uint8_t, SoftCreatSnesSeqEventType> EventMap;

private:
  void LoadEventMap();

  uint8_t headerAlignSize;
};

class SoftCreatSnesTrack : public SeqTrack {
public:
  SoftCreatSnesTrack(SoftCreatSnesSeq *parentFile, int32_t offset = 0,
                     int32_t length = 0);
  void ResetVars() override;
  bool ReadEvent() override;
};
