#pragma once
#include <cstdint> // for uint32_t
#include <string>  // for wstring

#include "AkaoScanner.h" // for AkaoScanner
#include "Format.h"      // for Format, BEGIN_FORMAT, END_FORMAT, USI...
#include "Matcher.h"     // for GetIdMatcher, Matcher (ptr only)
#include "VGMColl.h"     // for VGMColl

class AkaoInstrSet;
class VGMScanner;

// const THIS_FORMAT FMT_AKAO;

// ********
// AkaoColl
// ********

class AkaoColl : public VGMColl {
public:
  AkaoColl(std::wstring name = L"Unnamed Collection") : VGMColl(name) {}
  ~AkaoColl() override = default;

  bool LoadMain() override;
  bool PreDLSMainCreation() override;
  bool PostDLSMainCreation() override;

public:
  AkaoInstrSet *origInstrSet;
  uint32_t numAddedInstrs;
};

// ***********
// AkaoMatcher
// ***********

// class AkaoMatcher : public SimpleMatcher
//{
// public:
//	AkaoMatcher(Format* format);
//	~AkaoMatcher(void) {}
//};

// **********
// AkaoFormat
// **********

// class AkaoFormat : public Format
//{
// public:
//	AkaoFormat(void) {}
//	virtual ~AkaoFormat(void) {}
BEGIN_FORMAT(Akao)
USING_SCANNER(AkaoScanner)
// USING_MATCHER_WITH_ARG(SimpleMatcher, true)
USING_MATCHER_WITH_ARG(GetIdMatcher, true)
USING_COLL(AkaoColl)
END_FORMAT()
//};
