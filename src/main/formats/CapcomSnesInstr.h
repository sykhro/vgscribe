#pragma once
#include <cstdint> // for uint32_t, uint8_t
#include <string>  // for wstring
#include <vector>  // for vector

#include "VGMInstrSet.h" // for VGMInstr, VGMInstrSet
#include "VGMRgn.h"      // for VGMRgn
#include "VGMSampColl.h"

class RawFile;

// ****************
// CapcomSnesInstrSet
// ****************

class CapcomSnesInstrSet : public VGMInstrSet {
public:
  CapcomSnesInstrSet(RawFile *file, uint32_t offset, uint32_t spcDirAddr,
                     const std::wstring &name = L"CapcomSnesInstrSet");
  ~CapcomSnesInstrSet() override;

  bool GetHeaderInfo() override;
  bool GetInstrPointers() override;

protected:
  uint32_t spcDirAddr;
  std::vector<uint8_t> usedSRCNs;
};

// *************
// CapcomSnesInstr
// *************

class CapcomSnesInstr : public VGMInstr {
public:
  CapcomSnesInstr(VGMInstrSet *instrSet, uint32_t offset, uint32_t theBank,
                  uint32_t theInstrNum, uint32_t spcDirAddr,
                  const std::wstring &name = L"CapcomSnesInstr");
  ~CapcomSnesInstr() override;

  bool LoadInstr() override;

  static bool IsValidHeader(RawFile *file, uint32_t addrInstrHeader,
                            uint32_t spcDirAddr, bool validateSample);

protected:
  uint32_t spcDirAddr;
};

// ***********
// CapcomSnesRgn
// ***********

class CapcomSnesRgn : public VGMRgn {
public:
  CapcomSnesRgn(CapcomSnesInstr *instr, uint32_t offset);
  ~CapcomSnesRgn() override;

  bool LoadRgn() override;
};
