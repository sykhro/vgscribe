#pragma once
#include "BytePattern.h"
#include "Scanner.h" // for USE_EXTENSION, VGMScanner

class BytePattern;
class RawFile;

class PandoraBoxSnesScanner : public VGMScanner {
public:
  PandoraBoxSnesScanner() { USE_EXTENSION(L"spc"); }
  ~PandoraBoxSnesScanner() override = default;

  void Scan(RawFile *file, void *info = nullptr) override;
  void SearchForPandoraBoxSnesFromARAM(RawFile *file);
  void SearchForPandoraBoxSnesFromROM(RawFile *file);

private:
  static BytePattern ptnLoadSeqKKO;
  static BytePattern ptnLoadSeqTSP;
  static BytePattern ptnSetDIR;
  static BytePattern ptnLoadSRCN;
};
