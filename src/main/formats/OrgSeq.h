#pragma once
#include <cstdint> // for int32_t
#include <cstdint> // for uint8_t, uint16_t, uint32_t

#include "OrgFormat.h"
#include "OrgScanner.h"
#include "SeqTrack.h" // for SeqTrack
#include "VGMSeq.h"   // for VGMSeq

class RawFile;

class OrgSeq : public VGMSeq {
public:
  OrgSeq(RawFile *file, uint32_t offset);

public:
  ~OrgSeq() override;

  bool GetHeaderInfo() override;

public:
  uint16_t waitTime; // I believe this is the millis per tick
  uint8_t beatsPerMeasure;
};

class OrgTrack : public SeqTrack {
public:
  OrgTrack(OrgSeq *parentFile, int32_t offset, int32_t length, uint8_t realTrk);

  virtual bool LoadTrack(uint32_t trackNum, uint32_t stopOffset,
                         int32_t stopDelta);
  bool ReadEvent() override;

public:
  uint8_t prevPan;

  uint16_t curNote;
  uint8_t realTrkNum;
  uint16_t freq;
  uint8_t waveNum;
  uint16_t numNotes;
};
