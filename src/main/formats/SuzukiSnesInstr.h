#pragma once
#include <cstdint> // for uint16_t, uint32_t, uint8_t
#include <string>  // for wstring
#include <vector>  // for vector

#include "SuzukiSnesFormat.h" // for SuzukiSnesVersion
#include "VGMInstrSet.h"      // for VGMInstr, VGMInstrSet
#include "VGMRgn.h"           // for VGMRgn
#include "VGMSampColl.h"

class RawFile;

// ******************
// SuzukiSnesInstrSet
// ******************

class SuzukiSnesInstrSet : public VGMInstrSet {
public:
  SuzukiSnesInstrSet(RawFile *file, SuzukiSnesVersion ver, uint32_t spcDirAddr,
                     uint16_t addrSRCNTable, uint16_t addrVolumeTable,
                     uint16_t addrADSRTable, uint16_t addrTuningTable,
                     const std::wstring &name = L"SuzukiSnesInstrSet");
  ~SuzukiSnesInstrSet() override;

  bool GetHeaderInfo() override;
  bool GetInstrPointers() override;

  SuzukiSnesVersion version;

protected:
  uint32_t spcDirAddr;
  uint16_t addrSRCNTable;
  uint16_t addrVolumeTable;
  uint16_t addrTuningTable;
  uint16_t addrADSRTable;
  std::vector<uint8_t> usedSRCNs;
};

// *************
// SuzukiSnesInstr
// *************

class SuzukiSnesInstr : public VGMInstr {
public:
  SuzukiSnesInstr(VGMInstrSet *instrSet, SuzukiSnesVersion ver,
                  uint8_t instrNum, uint32_t spcDirAddr, uint16_t addrSRCNTable,
                  uint16_t addrVolumeTable, uint16_t addrADSRTable,
                  uint16_t addrTuningTable,
                  const std::wstring &name = L"SuzukiSnesInstr");
  ~SuzukiSnesInstr() override;

  bool LoadInstr() override;

  SuzukiSnesVersion version;

protected:
  uint32_t spcDirAddr;
  uint16_t addrSRCNTable;
  uint16_t addrVolumeTable;
  uint16_t addrTuningTable;
  uint16_t addrADSRTable;
};

// *************
// SuzukiSnesRgn
// *************

class SuzukiSnesRgn : public VGMRgn {
public:
  SuzukiSnesRgn(SuzukiSnesInstr *instr, SuzukiSnesVersion ver, uint8_t instrNum,
                uint32_t spcDirAddr, uint16_t addrSRCNTable,
                uint16_t addrVolumeTable, uint16_t addrADSRTable,
                uint16_t addrTuningTable);
  ~SuzukiSnesRgn() override;

  bool LoadRgn() override;

  SuzukiSnesVersion version;
};
