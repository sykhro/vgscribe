#pragma once
#include <cstdint> // for int32_t
#include <cstdint> // for uint16_t, uint32_t, uint8_t
#include <map>     // for map

#include "NeverlandSnesFormat.h" // for NeverlandSnesVersion
#include "SeqEvent.h"
#include "SeqTrack.h" // for SeqTrack
#include "VGMSeq.h"   // for VGMSeq

class RawFile;

enum NeverlandSnesSeqEventType {
  // start enum at 1 because if map[] look up fails, it returns 0, and we don't
  // want that to get confused with a legit event
  EVENT_UNKNOWN0 = 1,
  EVENT_UNKNOWN1,
  EVENT_UNKNOWN2,
  EVENT_UNKNOWN3,
  EVENT_UNKNOWN4,
};

class NeverlandSnesSeq : public VGMSeq {
public:
  NeverlandSnesSeq(RawFile *file, NeverlandSnesVersion ver,
                   uint32_t seqdataOffset);
  ~NeverlandSnesSeq() override;

  bool GetHeaderInfo() override;
  bool GetTrackPointers() override;
  void ResetVars() override;

  NeverlandSnesVersion version;
  std::map<uint8_t, NeverlandSnesSeqEventType> EventMap;

  uint16_t ConvertToAPUAddress(uint16_t offset);
  uint16_t GetShortAddress(uint32_t offset);

private:
  void LoadEventMap();
};

class NeverlandSnesTrack : public SeqTrack {
public:
  NeverlandSnesTrack(NeverlandSnesSeq *parentFile, int32_t offset = 0,
                     int32_t length = 0);
  void ResetVars() override;
  bool ReadEvent() override;

  uint16_t ConvertToAPUAddress(uint16_t offset);
  uint16_t GetShortAddress(uint32_t offset);
};
