#pragma once
#include <cstdint> // for int32_t
#include <cstdint> // for uint16_t, uint32_t

#include "SeqTrack.h" // for SeqTrack
#include "SquarePS2Format.h"
#include "VGMSeq.h" // for VGMSeq

class RawFile;

class BGMSeq : public VGMSeq {
public:
  BGMSeq(RawFile *file, uint32_t offset);
  ~BGMSeq() override;

  bool GetHeaderInfo() override;
  bool GetTrackPointers() override;
  uint32_t GetID() override { return assocWDID; }

protected:
  uint16_t seqID;
  uint16_t assocWDID;
};

class BGMTrack : public SeqTrack {
public:
  BGMTrack(BGMSeq *parentSeq, int32_t offset = 0, int32_t length = 0);

  bool ReadEvent() override;
};
