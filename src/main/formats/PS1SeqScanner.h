#pragma once
#include <vector> // for vector

#include "PS1Seq.h"
#include "Scanner.h" // for VGMScanner
#include "Vab.h"

class PS1Seq;
class RawFile;
class Vab;

class PS1SeqScanner : public VGMScanner {
public:
  PS1SeqScanner();
  ~PS1SeqScanner() override;

  void Scan(RawFile *file, void *info = nullptr) override;
  std::vector<PS1Seq *> SearchForPS1Seq(RawFile *file);
  std::vector<Vab *> SearchForVab(RawFile *file);
};
