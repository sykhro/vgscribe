#pragma once
#include <cstdint> // for uint32_t, uint8_t

#include "VGMSeqNoTrks.h" // for VGMSeqNoTrks

class RawFile;

class PS1Seq : public VGMSeqNoTrks {
public:
  PS1Seq(RawFile *file, uint32_t offset);
  ~PS1Seq() override;

  bool GetHeaderInfo() override;
  void ResetVars() override;
  bool ReadEvent() override;

protected:
  uint8_t runningStatus;
};
