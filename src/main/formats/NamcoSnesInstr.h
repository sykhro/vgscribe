#pragma once
#include <cstdint> // for uint16_t, uint32_t, uint8_t
#include <string>  // for wstring
#include <vector>  // for vector

#include "NamcoSnesFormat.h" // for NamcoSnesVersion
#include "VGMInstrSet.h"     // for VGMInstr, VGMInstrSet
#include "VGMRgn.h"          // for VGMRgn
#include "VGMSampColl.h"

class RawFile;

// *****************
// NamcoSnesInstrSet
// *****************

class NamcoSnesInstrSet : public VGMInstrSet {
public:
  NamcoSnesInstrSet(RawFile *file, NamcoSnesVersion ver, uint32_t spcDirAddr,
                    uint16_t addrTuningTable,
                    const std::wstring &name = L"NamcoSnesInstrSet");
  ~NamcoSnesInstrSet() override;

  bool GetHeaderInfo() override;
  bool GetInstrPointers() override;

  NamcoSnesVersion version;

protected:
  uint32_t spcDirAddr;
  uint16_t addrTuningTable;
  std::vector<uint8_t> usedSRCNs;
};

// **************
// NamcoSnesInstr
// **************

class NamcoSnesInstr : public VGMInstr {
public:
  NamcoSnesInstr(VGMInstrSet *instrSet, NamcoSnesVersion ver, uint8_t srcn,
                 uint32_t spcDirAddr, uint16_t addrTuningEntry,
                 const std::wstring &name = L"NamcoSnesInstr");
  ~NamcoSnesInstr() override;

  bool LoadInstr() override;

  NamcoSnesVersion version;

protected:
  uint32_t spcDirAddr;
  uint16_t addrTuningEntry;
};

// ************
// NamcoSnesRgn
// ************

class NamcoSnesRgn : public VGMRgn {
public:
  NamcoSnesRgn(NamcoSnesInstr *instr, NamcoSnesVersion ver, uint8_t srcn,
               uint32_t spcDirAddr, uint16_t addrTuningEntry);
  ~NamcoSnesRgn() override;

  bool LoadRgn() override;

  NamcoSnesVersion version;
};
