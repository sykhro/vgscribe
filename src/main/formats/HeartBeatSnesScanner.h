#pragma once
#include "BytePattern.h"
#include "Scanner.h" // for USE_EXTENSION, VGMScanner

class BytePattern;
class RawFile;

enum HeartBeatSnesVersion : uint8_t; // see HeartBeatSnesFormat.h

class HeartBeatSnesScanner : public VGMScanner {
public:
  HeartBeatSnesScanner() { USE_EXTENSION(L"spc"); }
  ~HeartBeatSnesScanner() override = default;

  void Scan(RawFile *file, void *info = nullptr) override;
  void SearchForHeartBeatSnesFromARAM(RawFile *file);
  void SearchForHeartBeatSnesFromROM(RawFile *file);

private:
  static BytePattern ptnReadSongList;
  static BytePattern ptnSetDIR;
  static BytePattern ptnLoadSRCN;
  static BytePattern ptnSaveSeqHeaderAddress;
};
