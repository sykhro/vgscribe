#include <cstdint> // for int16_t

#include "NDSSeq.h"

class RawFile;

DECLARE_FORMAT(NDS);

NDSSeq::NDSSeq(RawFile *file, uint32_t offset, uint32_t length,
               std::wstring name)
    : VGMSeq(NDSFormat::name, file, offset, length, name) {}

bool NDSSeq::GetHeaderInfo() {
  VGMHeader *SSEQHdr = AddHeader(dwOffset, 0x10, L"SSEQ Header");
  SSEQHdr->AddSig(dwOffset, 4);
  SSEQHdr->AddSimpleItem(dwOffset + 4, 2, L"Endianness");
  SSEQHdr->AddSimpleItem(dwOffset + 6, 2, L"Version");
  SSEQHdr->AddSimpleItem(dwOffset + 8, 4, L"Size");
  SSEQHdr->AddSimpleItem(dwOffset + 12, 2, L"Header size");
  SSEQHdr->AddSimpleItem(dwOffset + 14, 2, L"Data chunks");

  unLength = GetShort(dwOffset + 8);

  /* When the tempo is 240, the length of one produced sound
     is equal to the length of one tick */
  SetPPQN(48);

  return true;
}

bool NDSSeq::GetTrackPointers() {
  VGMHeader *DATAHdr = AddHeader(dwOffset + 0x10, 0xC, L"DATA Chunk header");
  DATAHdr->AddSig(dwOffset + 0x10, 4);
  DATAHdr->AddSimpleItem(dwOffset + 0x10 + 4, 4, L"Size");
  DATAHdr->AddSimpleItem(dwOffset + 0x10 + 8, 4, L"Data Pointer");
  uint32_t offset = dwOffset + 0x1C;
  uint8_t b = GetByte(offset);
  aTracks.emplace_back(new NDSTrack(this));

  if (b == SEQ_MULTITRACK) {
    VGMHeader *TrkPtrs = AddHeader(offset, 0, L"Track Pointers");
    TrkPtrs->AddSimpleItem(offset, 3, L"Valid Tracks");
    offset += 3;
    b = GetByte(offset);
    uint32_t songDelay = 0;

    while (b == SEQ_CMD_WAIT) {
      uint32_t value;
      uint8_t c;
      uint32_t beginOffset = offset;
      offset++;

      if ((value = GetByte(offset++)) & SEQ_CMD_WAIT) {
        value &= 0x7F;
        do {
          value = (value << 7) + ((c = GetByte(offset++)) & 0x7F);
        } while (c & SEQ_CMD_WAIT);
      }

      songDelay += value;
      TrkPtrs->AddSimpleItem(beginOffset, offset - beginOffset, L"Delay");
      b = GetByte(offset);

      break;
    }

    // Track/Channel assignment and pointer.  Channel # is irrelevant
    while (b == SEQ_CMD_OPEN_TRACK) {
      TrkPtrs->AddSimpleItem(offset, 5, L"Track Pointer");
      uint32_t trkOffset = GetByte(offset + 2) + (GetByte(offset + 3) << 8) +
                           (GetByte(offset + 4) << 16) + dwOffset + 0x1C;
      aTracks.emplace_back(new NDSTrack(this, trkOffset));
      offset += 5;
      b = GetByte(offset);
    }
    TrkPtrs->unLength = offset - TrkPtrs->dwOffset;
  }

  aTracks[0]->dwOffset = offset;
  aTracks[0]->dwStartOffset = offset;
  return true;
}

/*
 * NDSTrack
 */

NDSTrack::NDSTrack(NDSSeq *parentFile, uint32_t offset, uint32_t length)
    : SeqTrack(parentFile, offset, length) {
  ResetVars();
  bDetermineTrackLengthEventByEvent = true;
}

void NDSTrack::ResetVars() {
  jumpCount = 0;
  loopReturnOffset = 0;
  hasLoopReturnOffset = false;
  SeqTrack::ResetVars();
}

bool NDSTrack::ReadEvent() {
  uint32_t beginOffset = curOffset;
  uint8_t status_byte = GetByte(curOffset++);

  // Note ON
  if (status_byte < SEQ_CMD_WAIT) {
    uint8_t vel = GetByte(curOffset++);
    dur = ReadVarLen(curOffset);
    AddNoteByDur(beginOffset, curOffset - beginOffset, status_byte, vel, dur);

    if (noteWithDelta) {
      AddTime(dur);
    }
  } else {
    switch (status_byte) {
    case SEQ_CMD_WAIT:
      dur = ReadVarLen(curOffset);
      AddRest(beginOffset, curOffset - beginOffset, dur);
      break;

    case SEQ_CMD_PRG: {
      auto newProg = static_cast<uint8_t>(ReadVarLen(curOffset));
      AddProgramChange(beginOffset, curOffset - beginOffset, newProg);
      break;
    }

    case SEQ_CMD_OPEN_TRACK:
      curOffset += 4;
      AddUnknown(beginOffset, curOffset - beginOffset, L"Open Track");
      break;

    case SEQ_CMD_JUMP: {
      uint32_t jumpAddr = GetByte(curOffset) + (GetByte(curOffset + 1) << 8) +
                          (GetByte(curOffset + 2) << 16) + parentSeq->dwOffset +
                          0x1C;
      curOffset += 3;

      // Add an End Track if it exists afterward, for completeness sake
      if (readMode == READMODE_ADD_TO_UI && !IsOffsetUsed(curOffset)) {
        if (GetByte(curOffset) == 0xFF) {
          AddGenericEvent(curOffset, 1, L"End of Track", L"", CLR_TRACKEND,
                          ICON_TRACKEND);
        }
      }

      // The event usually appears at last of the song, but there can be an
      // exception. See Zelda The Spirit Tracks - SSEQ_0018 (overworld train
      // theme)
      bool bContinue = true;
      if (IsOffsetUsed(jumpAddr)) {
        bContinue = AddLoopForever(beginOffset, 4, L"Loop");
      } else {
        AddGenericEvent(beginOffset, 4, L"Jump", L"", CLR_LOOPFOREVER);
      }

      curOffset = jumpAddr;
      return bContinue;
    }

    case SEQ_CMD_CALL:
      hasLoopReturnOffset = true;
      loopReturnOffset = curOffset + 3;
      AddGenericEvent(beginOffset, curOffset + 3 - beginOffset, L"Call", L"",
                      CLR_LOOP);
      curOffset = GetByte(curOffset) + (GetByte(curOffset + 1) << 8) +
                  (GetByte(curOffset + 2) << 16) + parentSeq->dwOffset + 0x1C;
      break;

    case SEQ_CMD_RANDOM: {
      uint8_t subStatusByte;
      int16_t randMin;
      int16_t randMax;

      subStatusByte = GetByte(curOffset++);
      randMin = static_cast<signed>(GetShort(curOffset));
      curOffset += 2;
      randMax = static_cast<signed>(GetShort(curOffset));
      curOffset += 2;

      AddUnknown(beginOffset, curOffset - beginOffset,
                 L"Cmd with Random Value");
      break;
    }

    case SEQ_CMD_VARIABLE: {
      uint8_t subStatusByte = GetByte(curOffset++);
      uint8_t varNumber = GetByte(curOffset++);

      AddUnknown(beginOffset, curOffset - beginOffset, L"Cmd with Variable");
      break;
    }

    case SEQ_CMD_IF: {
      AddUnknown(beginOffset, curOffset - beginOffset, L"Conditional block");
      break;
    }

    case SEQ_CMD_SETVAR:
    case SEQ_CMD_ADDVAR:
    case SEQ_CMD_SUBVAR:
    case SEQ_CMD_MULVAR:
    case SEQ_CMD_DIVVAR:
    case SEQ_CMD_SHIFTVAR:
    case SEQ_CMD_RANDVAR:
    case SEQ_CMD_CMP_EQ:
    case SEQ_CMD_CMP_GE:
    case SEQ_CMD_CMP_GT:
    case SEQ_CMD_CMP_LE:
    case SEQ_CMD_CMP_LT:
    case SEQ_CMD_CMP_NE: {
      uint8_t varNumber;
      int16_t val;
      const wchar_t *eventName[] = {L"Set Variable",   L"Add Variable",
                                    L"Sub Variable",   L"Mul Variable",
                                    L"Div Variable",   L"Shift Vabiable",
                                    L"Rand Variable",  L"",
                                    L"If Variable ==", L"If Variable >=",
                                    L"If Variable >",  L"If Variable <=",
                                    L"If Variable <",  L"If Variable !="};

      varNumber = GetByte(curOffset++);
      val = GetShort(curOffset);
      curOffset += 2;

      AddUnknown(beginOffset, curOffset - beginOffset,
                 eventName[status_byte - 0xB0]);
      break;
    }

    case SEQ_CMD_PAN: {
      uint8_t pan = GetByte(curOffset++);
      AddPan(beginOffset, curOffset - beginOffset, pan);
      break;
    }

    case SEQ_CMD_VOLUME:
      vol = GetByte(curOffset++);
      AddVol(beginOffset, curOffset - beginOffset, vol);
      break;

    case SEQ_CMD_MAIN_VOLUME: {
      uint8_t mvol = GetByte(curOffset++);
      AddUnknown(beginOffset, curOffset - beginOffset, L"Master volume");
      break;
    }

    case SEQ_CMD_TRANSPOSE: {
      int8_t transpose = static_cast<signed>(GetByte(curOffset++));
      AddTranspose(beginOffset, curOffset - beginOffset, transpose);
      break;
    }

    case SEQ_CMD_PITCH_BEND: {
      int16_t bend = static_cast<signed>(GetByte(curOffset++)) * 64;
      AddPitchBend(beginOffset, curOffset - beginOffset, bend);
      break;
    }

    case SEQ_CMD_BEND_RANGE: {
      uint8_t semitones = GetByte(curOffset++);
      AddPitchBendRange(beginOffset, curOffset - beginOffset, semitones);
      break;
    }

    case SEQ_CMD_PRIO:
      curOffset++;
      AddGenericEvent(beginOffset, curOffset - beginOffset, L"Priority", L"",
                      CLR_CHANGESTATE);
      break;

    case SEQ_CMD_NOTE_WAIT: {
      uint8_t notewait = GetByte(curOffset++);
      noteWithDelta = (notewait != 0);
      AddUnknown(beginOffset, curOffset - beginOffset, L"Notewait Mode");
      break;
    }

    case SEQ_CMD_TIE:
      curOffset++;
      AddUnknown(beginOffset, curOffset - beginOffset, L"Tie");
      break;

    case SEQ_CMD_PORTA:
      curOffset++;
      AddUnknown(beginOffset, curOffset - beginOffset, L"Portamento Control");
      break;

    case SEQ_CMD_MOD_DEPTH: {
      uint8_t amount = GetByte(curOffset++);
      AddModulation(beginOffset, curOffset - beginOffset, amount,
                    L"Modulation Depth");
      break;
    }

    case SEQ_CMD_MOD_SPEED:
      curOffset++;
      AddUnknown(beginOffset, curOffset - beginOffset, L"Modulation Speed");
      break;

    case SEQ_CMD_MOD_TYPE:
      curOffset++;
      AddUnknown(beginOffset, curOffset - beginOffset, L"Modulation Type");
      break;

    case SEQ_CMD_MOD_RANGE:
      curOffset++;
      AddUnknown(beginOffset, curOffset - beginOffset, L"Modulation Range");
      break;

    case SEQ_CMD_PORTA_SW: {
      bool bPortOn = (GetByte(curOffset++) != 0);
      AddPortamento(beginOffset, curOffset - beginOffset, bPortOn);
      break;
    }

    case SEQ_CMD_PORTA_TIME: {
      uint8_t portTime = GetByte(curOffset++);
      AddPortamentoTime(beginOffset, curOffset - beginOffset, portTime);
      break;
    }

    case SEQ_CMD_ATTACK:
      curOffset++;
      AddUnknown(beginOffset, curOffset - beginOffset, L"Attack Rate");
      break;

    case SEQ_CMD_DECAY:
      curOffset++;
      AddUnknown(beginOffset, curOffset - beginOffset, L"Decay Rate");
      break;

    case SEQ_CMD_SUSTAIN:
      curOffset++;
      AddUnknown(beginOffset, curOffset - beginOffset, L"Sustain Level");
      break;

    case SEQ_CMD_RELEASE:
      curOffset++;
      AddUnknown(beginOffset, curOffset - beginOffset, L"Release Rate");
      break;

    case SEQ_CMD_LOOP_START:
      curOffset++;
      AddUnknown(beginOffset, curOffset - beginOffset, L"Loop Start");
      break;

    case SEQ_CMD_VOLUME2: {
      uint8_t expression = GetByte(curOffset++);
      AddExpression(beginOffset, curOffset - beginOffset, expression);
      break;
    }

    case SEQ_CMD_PRINTVAR:
      curOffset++;
      AddUnknown(beginOffset, curOffset - beginOffset, L"Print Variable");
      break;

    case SEQ_CMD_MOD_DELAY:
      curOffset += 2;
      AddUnknown(beginOffset, curOffset - beginOffset, L"Modulation Delay");
      break;

    case SEQ_CMD_TEMPO: {
      uint16_t bpm = GetShort(curOffset);
      curOffset += 2;
      AddTempoBPM(beginOffset, curOffset - beginOffset, bpm);
      break;
    }

    case SEQ_CMD_SWEEP_PITCH:
      curOffset += 2;
      AddUnknown(beginOffset, curOffset - beginOffset, L"Sweep Pitch");
      break;

    case SEQ_CMD_LOOP_END:
      AddUnknown(beginOffset, curOffset - beginOffset, L"Loop End");
      break;

    case SEQ_CMD_RET: {
      // This event usually does not cause an infinite loop.
      // However, a complicated sequence with a ton of conditional events, it
      // sometimes confuses the parser and causes an infinite loop. See Animal
      // Crossing: Wild World - SSEQ_270
      bool bContinue = true;
      if (!hasLoopReturnOffset || IsOffsetUsed(loopReturnOffset)) {
        bContinue = false;
      }

      AddGenericEvent(beginOffset, curOffset - beginOffset, L"Return", L"",
                      CLR_LOOP);
      curOffset = loopReturnOffset;
      return bContinue;
    }

    case SEQ_CMD_ALLOC_TRACK:
      curOffset += 2;
      AddUnknown(beginOffset, curOffset - beginOffset, L"Allocate Track");
      break;

    case SEQ_CMD_FIN:
      AddEndOfTrack(beginOffset, curOffset - beginOffset);
      return false;

    case SEQ_CMD_MUTE: // FIXME: Implement mute
    default:
      AddUnknown(beginOffset, curOffset - beginOffset);
      return false;
    }
  }

  return true;
}
