#pragma once
#include "BytePattern.h"
#include "Scanner.h" // for USE_EXTENSION, VGMScanner

class BytePattern;
class RawFile;

enum HudsonSnesVersion : uint8_t; // see HudsonSnesFormat.h

class HudsonSnesScanner : public VGMScanner {
public:
  HudsonSnesScanner() { USE_EXTENSION(L"spc"); }
  ~HudsonSnesScanner() override = default;

  void Scan(RawFile *file, void *info = nullptr) override;
  void SearchForHudsonSnesFromARAM(RawFile *file);
  void SearchForHudsonSnesFromROM(RawFile *file);

private:
  static BytePattern ptnNoteLenTable;
  static BytePattern ptnGetSeqTableAddrV0;
  static BytePattern ptnGetSeqTableAddrV1V2;
  static BytePattern ptnLoadTrackAddress;
  static BytePattern ptnLoadDIRV0;
};
