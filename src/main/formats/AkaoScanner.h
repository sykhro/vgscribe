#pragma once
#include "Scanner.h" // for VGMScanner

class RawFile;

class AkaoScanner : public VGMScanner {
public:
  AkaoScanner();

public:
  ~AkaoScanner() override;

  void Scan(RawFile *file, void *info = nullptr) override;
};
