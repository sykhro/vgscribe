#pragma once
#include <cstdint> // for uint32_t

#include "Scanner.h" // for USE_EXTENSION, VGMScanner

class RawFile;

class NDSScanner : public VGMScanner {
public:
  NDSScanner() {
    USE_EXTENSION(L"nds")
    USE_EXTENSION(L"sdat")
  }
  ~NDSScanner() override = default;

  void Scan(RawFile *file, void *info = nullptr) override;
  void SearchForSDAT(RawFile *file);
  uint32_t LoadFromSDAT(RawFile *file, uint32_t baseOff);
};
