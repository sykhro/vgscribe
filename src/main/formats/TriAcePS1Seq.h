#pragma once
#include <cstdint> // for int32_t
#include <cstdint> // for uint32_t, uint16_t, uint8_t
#include <map>     // for map
#include <string>  // for wstring
#include <vector>  // for vector

#include "SeqTrack.h" // for SeqTrack
#include "TriAcePS1Format.h"
#include "VGMItem.h" // for VGMContainerItem
#include "VGMSeq.h"  // for VGMSeq

class RawFile;
class SeqEvent;
class TriAcePS1ScorePattern;
class VGMHeader;

class TriAcePS1Seq : public VGMSeq {
public:
  typedef struct _TrkInfo {
    uint16_t unknown1;
    uint16_t unknown2;
    uint16_t trkOffset;
  } TrkInfo;

  TriAcePS1Seq(RawFile *file, uint32_t offset,
               const std::wstring &name = std::wstring(L"TriAce Seq"));
  ~TriAcePS1Seq() override;

  bool GetHeaderInfo() override;
  bool GetTrackPointers() override;
  void ResetVars() override;

  VGMHeader *header;
  TrkInfo TrkInfos[32];
  std::vector<TriAcePS1ScorePattern *> aScorePatterns;
  TriAcePS1ScorePattern *curScorePattern;
  std::map<uint32_t, TriAcePS1ScorePattern *> patternMap;
  uint8_t initialTempoBPM;
};

class TriAcePS1ScorePattern : public VGMContainerItem {
public:
  TriAcePS1ScorePattern(TriAcePS1Seq *parentSeq, uint32_t offset)
      : VGMContainerItem(parentSeq, offset, 0, L"Score Pattern") {}
};

class TriAcePS1Track : public SeqTrack {
public:
  TriAcePS1Track(TriAcePS1Seq *parentSeq, int32_t offset = 0,
                 int32_t length = 0);

  void LoadTrackMainLoop(uint32_t stopOffset, int32_t stopTime) override;
  uint32_t ReadScorePattern(uint32_t offset);
  bool IsOffsetUsed(uint32_t offset) override;
  void AddEvent(SeqEvent *pSeqEvent) override;
  bool ReadEvent() override;

  uint8_t impliedNoteDur;
  uint8_t impliedVelocity;
};
