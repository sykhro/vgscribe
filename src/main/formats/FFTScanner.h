#pragma once
#include "Scanner.h" // for VGMScanner

class RawFile;

class FFTScanner : public VGMScanner {
public:
  FFTScanner();

public:
  ~FFTScanner() override;

  void Scan(RawFile *file,
            void *info = nullptr) override; // scan "smds" and "wds"
  void SearchForFFTSeq(RawFile *file);      // scan "smds"
  void SearchForFFTwds(RawFile *file);      // scan "wds"
};
