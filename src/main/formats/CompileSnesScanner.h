#pragma once
#include "BytePattern.h"
#include "Scanner.h" // for USE_EXTENSION, VGMScanner

class BytePattern;
class RawFile;

enum CompileSnesVersion : uint8_t; // see CompileSnesFormat.h

class CompileSnesScanner : public VGMScanner {
public:
  CompileSnesScanner() { USE_EXTENSION(L"spc"); }
  ~CompileSnesScanner() override = default;

  void Scan(RawFile *file, void *info = nullptr) override;
  void SearchForCompileSnesFromARAM(RawFile *file);
  void SearchForCompileSnesFromROM(RawFile *file);

private:
  static BytePattern ptnSetSongListAddress;
};
