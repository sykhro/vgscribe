#pragma once

#include "RawFile.h"

class RawFile;

enum PostLoadCommand { KEEP_IT, DELETE_IT };

class VGMLoader {
public:
  VGMLoader();

public:
  virtual ~VGMLoader();

  virtual PostLoadCommand Apply(RawFile *theFile);
};
