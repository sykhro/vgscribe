#pragma once

enum LoopMeasure { LM_SAMPLES, LM_BYTES };

struct Loop {
  Loop() = default;

  int loopStatus{-1};
  uint32_t loopType{0};
  uint8_t loopStartMeasure{LM_BYTES};
  uint8_t loopLengthMeasure{LM_BYTES};
  uint32_t loopStart{0};
  uint32_t loopLength{0};
};
