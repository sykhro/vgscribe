#pragma once
#include <cstdint> // for int32_t
#include <cstdint> // for uint32_t, uint16_t, uint8_t
#include <string>  // for wstring, string
#include <vector>  // for vector

#include "SeqTrack.h" // for SeqTrack
#include "VGMSeq.h"   // for VGMSeq, ReadMode::READMODE_ADD_TO_UI

class MidiFile;
class MidiTrack;
class RawFile;

class VGMSeqNoTrks : public VGMSeq, public SeqTrack {
public:
  VGMSeqNoTrks(const std::string &format, RawFile *file, uint32_t offset,
               std::wstring name = L"VGM Sequence");

public:
  ~VGMSeqNoTrks() override;

  void ResetVars() override;

  inline uint32_t GetBytes(uint32_t nIndex, uint32_t nCount, void *pBuffer) {
    return VGMSeq::GetBytes(nIndex, nCount, pBuffer);
  }
  inline uint8_t GetByte(uint32_t offset) { return VGMSeq::GetByte(offset); }
  inline uint16_t GetShort(uint32_t offset) { return VGMSeq::GetShort(offset); }
  inline uint32_t GetWord(uint32_t offset) { return VGMSeq::GetWord(offset); }
  inline uint16_t GetShortBE(uint32_t offset) {
    return VGMSeq::GetShortBE(offset);
  }
  inline uint32_t GetWordBE(uint32_t offset) {
    return VGMSeq::GetWordBE(offset);
  }
  inline uint32_t &offset() { return VGMSeq::dwOffset; }
  inline uint32_t &length() { return VGMSeq::unLength; }
  inline std::wstring &name() { return VGMSeq::name; }

  inline uint32_t &eventsOffset() { return dwEventsOffset; }

  // this function must be called in GetHeaderInfo or before LoadEvents is
  // called
  inline void SetEventsOffset(int32_t offset) {
    dwEventsOffset = offset;
    if (SeqTrack::readMode == READMODE_ADD_TO_UI) {
      SeqTrack::dwOffset = offset;
    }
  }

  virtual void AddTime(uint32_t delta);

  void SetCurTrack(uint32_t trackNum);
  void TryExpandMidiTracks(uint32_t numTracks);

  bool LoadMain()
      override; // Function to load all the information about the sequence
  virtual bool LoadEvents(int32_t stopTime = 1000000);
  MidiFile *ConvertToMidi() override;
  MidiTrack *GetFirstMidiTrack() override;

  uint32_t dwEventsOffset;

protected:
  // an array of midi tracks... we will change pMidiTrack, which all the
  // SeqTrack functions write to, to the
  // corresponding MidiTrack in this vector before we write every event
  std::vector<MidiTrack *> midiTracks;
};
