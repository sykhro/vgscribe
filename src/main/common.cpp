#include "common.h"

std::wstring StringToLower(std::wstring myString) {
  const size_t length = myString.length();
  for (size_t i = 0; i != length; ++i) {
    myString[i] = tolower(myString[i]);
  }
  return myString;
}

uint32_t StringToHex(const std::string &str) {
  uint32_t value;
  std::stringstream convert(str);
  convert >> std::hex >> value; // read seq_table as hexadecimal value
  return value;
}

std::wstring ConvertToSafeFileName(const std::wstring &str) {
  std::wstring filename;
  filename.reserve(str.length());

  const wchar_t *forbiddenChars = L"\\/:,;*?\"<>|";
  size_t pos_begin = 0;
  size_t pos_end;
  while ((pos_end = str.find_first_of(forbiddenChars, pos_begin)) !=
         std::wstring::npos) {
    filename += str.substr(pos_begin, pos_end - pos_begin);
    if (filename[filename.length() - 1] != L' ') {
      filename += L" ";
    }
    pos_begin = pos_end + 1;
  }
  filename += str.substr(pos_begin);

  // right trim
  filename.erase(filename.find_last_not_of(L" \n\r\t") + 1);

  return filename;
}
