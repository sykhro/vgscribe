#include <cassert> // for assert

#include "Format.h" // for Format
#include "Root.h"
#include "VGMColl.h"                       // for VGMColl
#include "formats/AkaoScanner.h"           // for AkaoScanner
#include "formats/AkaoSnesScanner.h"       // for AkaoSnesScanner
#include "formats/CapcomSnesScanner.h"     // for CapcomSnesScanner
#include "formats/ChunSnesScanner.h"       // for ChunSnesScanner
#include "formats/CompileSnesScanner.h"    // for CompileSnesScanner
#include "formats/FFTScanner.h"            // for FFTScanner
#include "formats/FalcomSnesScanner.h"     // for FalcomSnesScanner
#include "formats/GraphResSnesScanner.h"   // for GraphResSnesScanner
#include "formats/HOSAScanner.h"           // for HOSAScanner
#include "formats/HeartBeatPS1Scanner.h"   // for HeartBeatPS1Scanner
#include "formats/HeartBeatSnesScanner.h"  // for HeartBeatSnesScanner
#include "formats/HudsonSnesScanner.h"     // for HudsonSnesScanner
#include "formats/KonamiSnesScanner.h"     // for KonamiSnesScanner
#include "formats/MP2kScanner.h"           // for MP2kScanner
#include "formats/MoriSnesScanner.h"       // for MoriSnesScanner
#include "formats/NDSScanner.h"            // for NDSScanner
#include "formats/NamcoSnesScanner.h"      // for NamcoSnesScanner
#include "formats/NeverlandSnesScanner.h"  // for NeverlandSnesScanner
#include "formats/NinSnesScanner.h"        // for NinSnesScanner
#include "formats/OrgScanner.h"            // for OrgScanner
#include "formats/PS1SeqScanner.h"         // for PS1SeqScanner
#include "formats/PandoraBoxSnesScanner.h" // for PandoraBoxSnesScanner
#include "formats/PrismSnesScanner.h"      // for PrismSnesScanner
#include "formats/RareSnesScanner.h"       // for RareSnesScanner
#include "formats/SoftCreatSnesScanner.h"  // for SoftCreatSnesScanner
#include "formats/SonyPS2Scanner.h"        // for SonyPS2Scanner
#include "formats/SquarePS2Scanner.h"      // for SquarePS2Scanner
#include "formats/SuzukiSnesScanner.h"     // for SuzukiSnesScanner
#include "formats/TamSoftPS1Scanner.h"     // for TamSoftPS1Scanner
#include "formats/TriAcePS1Scanner.h"      // for TriAcePS1Scanner
#include "loaders/PSF2Loader.h"            // for PSF2Loader
#include "loaders/SFLoader.h"              // for SFLoader
#include "loaders/SPCLoader.h"             // for SPCLoader

#ifdef _MSC_VER
#include "loaders/MAMELoader.h"
#endif

VGMRoot *pRoot;

VGMRoot::VGMRoot() = default;

VGMRoot::~VGMRoot() {
  DeleteVect<VGMLoader>(vLoader);
  DeleteVect<VGMScanner>(vScanner);
  DeleteVect<RawFile>(vRawFile);
  DeleteVect<VGMFile>(vVGMFile);
  DeleteVect<LogItem>(vLogItem);
}

/*
 * Inits VGMRoot by pushing all the scanners
 * and loaders on the dedicated vectors.
 * TODO: Better solution to load all of this stuff
 */
bool VGMRoot::Init() {
  UI_SetRootPtr(&pRoot);

  AddScanner<AkaoSnesScanner>();
  AddScanner<CapcomSnesScanner>();
  AddScanner<ChunSnesScanner>();
  AddScanner<CompileSnesScanner>();
  AddScanner<FalcomSnesScanner>();
  AddScanner<GraphResSnesScanner>();
  AddScanner<HeartBeatSnesScanner>();
  AddScanner<HudsonSnesScanner>();
  AddScanner<KonamiSnesScanner>();
  AddScanner<MoriSnesScanner>();
  AddScanner<NamcoSnesScanner>();
  AddScanner<NeverlandSnesScanner>();
  AddScanner<NinSnesScanner>();
  AddScanner<PandoraBoxSnesScanner>();
  AddScanner<PrismSnesScanner>();
  AddScanner<RareSnesScanner>();
  AddScanner<SoftCreatSnesScanner>();
  AddScanner<SuzukiSnesScanner>();

  AddScanner<OrgScanner>();
  AddScanner<MP2kScanner>();
  // AddScanner<KonamiGXScanner>();
  // AddScanner<QSoundScanner>();
  AddScanner<HOSAScanner>();
  AddScanner<FFTScanner>();
  // AddScanner<SegSatScanner>();

  AddScanner<PS1SeqScanner>();
  AddScanner<TamSoftPS1Scanner>();
  AddScanner<TriAcePS1Scanner>();

  AddScanner<AkaoScanner>();
  AddScanner<HeartBeatPS1Scanner>();
  AddScanner<SonyPS2Scanner>();
  AddScanner<SquarePS2Scanner>();
  AddScanner<NDSScanner>();

  AddLoader<PSF2Loader>();
  AddLoader<SFLoader>();
  AddLoader<SPCLoader>();
  // AddLoader<MAMELoader>();

  return true;
}

/*
 * Resets the UI.
 */
void VGMRoot::Reset() { DeleteVect<RawFile>(vRawFile); }

/*
 * Closes the UI.
 */
void VGMRoot::Exit() {
  UI_PreExit();
  Reset();
  UI_Exit();
}

/*
 * Opens a new RawFile
 */
bool VGMRoot::OpenRawFile(const std::wstring &filename) {
  RawFile *newRawFile = new RawFile(static_cast<fs::path>(filename));
  if (!newRawFile->open()) {
    delete newRawFile;
    return false;
  }

  return SetupNewRawFile(newRawFile);
}

/*
 * Closes a RawFile.
 */
bool VGMRoot::CloseRawFile(RawFile *targFile) {
  if (targFile == nullptr) {
    return false;
  }
  auto iter = find(vRawFile.begin(), vRawFile.end(), targFile);
  if (iter != vRawFile.end()) {
    vRawFile.erase(iter);
  } else {
    pRoot->AddLogItem(
        new LogItem(std::wstring(L"Error: trying to delete a rawfile which "
                                 L"cannot be found in vRawFile."),
                    LOG_LEVEL_DEBUG, L"Root"));
  }

  delete targFile;
  return true;
}

/*
 * Creates a virtual file.
 * Same as a RawFile, but sits entirely in memory.
 * Used, for example, when decompressing PSF2 files.
 */
bool VGMRoot::CreateVirtFile(uint8_t *databuf, uint32_t fileSize,
                             const std::wstring &filename,
                             const std::wstring &parRawFileFullPath,
                             const VGMTag tag) {

  assert(fileSize != 0);
  VirtFile *newVirtFile = new VirtFile(databuf, fileSize, filename,
                                       parRawFileFullPath.c_str(), tag);
  if (newVirtFile == nullptr) {
    return false;
  }

  if (!SetupNewRawFile(newVirtFile)) {
    delete newVirtFile;
    return false;
  }

  return true;
}

/*
 * Applies all the loaders and scanners
 * to the opened RawFile.
 */
bool VGMRoot::SetupNewRawFile(RawFile *newRawFile) {
  newRawFile->SetProPreRatio(static_cast<float>(0.80));

  if (newRawFile->processFlags & PF_USELOADERS) {
    for (auto &i : vLoader) {
      if (i->Apply(newRawFile) == DELETE_IT) {
        delete newRawFile;
        return true;
      }
    }
  }

  if (newRawFile->processFlags & PF_USESCANNERS) {
    /* See if there is an extension discriminator */
    std::list<VGMScanner *> *lScanners =
        ExtensionDiscriminator::instance().GetScannerList(
            newRawFile->GetExtension());
    if (lScanners) {
      /* If there is, scan with all relevant scanners... */
      for (auto &lScanner : *lScanners) {
        lScanner->Scan(newRawFile);
      }
    } else {
      /* ...otherwise, use every scanner */
      for (auto &i : vScanner) {
        i->Scan(newRawFile);
      }
    }
  }

  if (newRawFile->containedVGMFiles.empty()) {
    delete newRawFile;
    return true;
  }

  newRawFile->SetProPreRatio(0.5);
  vRawFile.emplace_back(newRawFile);
  UI_AddRawFile(newRawFile);
  return true;
}

/*
 * Adds a a VGMFile to the interface. The UI_AddVGMFile function will handle the
 * interface-specific stuff
 */
void VGMRoot::AddVGMFile(VGMFile *theFile) {
  theFile->GetRawFile()->AddContainedVGMFile(theFile);
  vVGMFile.emplace_back(theFile);
  UI_AddVGMFile(theFile);
}

/*
 * Removes a VGMFile from the interface. The UI_RemoveVGMFile will handle the
 * interface-specific stuff
 */
void VGMRoot::RemoveVGMFile(VGMFile *targFile, bool bRemoveFromRaw) {
  /* First we should call the format's onClose handler in case it needs to use
     the RawFile before we close it (FilenameMatcher, for ex) */
  Format *fmt = targFile->GetFormat();
  if (fmt) {
    fmt->OnCloseFile(targFile);
  }

  auto iter = find(vVGMFile.begin(), vVGMFile.end(), targFile);
  if (iter != vVGMFile.end()) {
    vVGMFile.erase(iter);
  } else {
    pRoot->AddLogItem(
        new LogItem(std::wstring(L"Error: trying to delete a VGMFile which "
                                 L"cannot be found in vVGMFile."),
                    LOG_LEVEL_DEBUG, L"Root"));
  }
  if (bRemoveFromRaw) {
    targFile->rawfile->RemoveContainedVGMFile(targFile);
  }
  while (!targFile->assocColls.empty()) {
    RemoveVGMColl(targFile->assocColls.back());
  }

  UI_RemoveVGMFile(targFile);
  delete targFile;
}

/*
 * Adds a VGM collection to the UI.
 */
void VGMRoot::AddVGMColl(VGMColl *theColl) {
  vVGMColl.emplace_back(theColl);
  UI_AddVGMColl(theColl);
}

/*
 * Removes a VGM collection from the UI.
 */
void VGMRoot::RemoveVGMColl(VGMColl *targColl) {
  targColl->RemoveFileAssocs();
  auto iter = find(vVGMColl.begin(), vVGMColl.end(), targColl);

  if (iter != vVGMColl.end()) {
    vVGMColl.erase(iter);
  } else {
    pRoot->AddLogItem(
        new LogItem(std::wstring(L"Error: trying to delete a VGMColl which "
                                 L"cannot be found in vVGMColl."),
                    LOG_LEVEL_DEBUG, L"Root"));
  }

  UI_RemoveVGMColl(targColl);
  delete targColl;
}

/*
 * This virtual function is called whenever a VGMFile is added to the interface.
 * By default, it simply sorts out what type of file was added and then calls a
 * more specific virtual function for the file type. It is virtual in case a
 * user-interface wants do something universally whenever any type of VGMFiles
 * is added.
 */
void VGMRoot::UI_AddVGMFile(VGMFile *theFile) {
  switch (theFile->GetFileType()) {
  case FILETYPE_SEQ:
    UI_AddVGMSeq(dynamic_cast<VGMSeq *>(theFile));
    break;
  case FILETYPE_INSTRSET:
    UI_AddVGMInstrSet(dynamic_cast<VGMInstrSet *>(theFile));
    break;
  case FILETYPE_SAMPCOLL:
    UI_AddVGMSampColl(dynamic_cast<VGMSampColl *>(theFile));
    break;
  case FILETYPE_MISC:
    UI_AddVGMMisc(reinterpret_cast<VGMMiscFile *>(theFile));
    break;
  }
}

/*
 * Adds a log item to the interface.  The UI_AddLog function will handle the
 * interface-specific stuff
 */
void VGMRoot::AddLogItem(LogItem *theLog) {
  vLogItem.emplace_back(theLog);
  UI_AddLogItem(theLog);
}

/*
 * Given a pointer to a buffer of data, size, and a filename, this function
 * writes the data into a file on the filesystem.
 */
bool VGMRoot::UI_WriteBufferToFile(std::experimental::filesystem::path filepath,
                                   uint8_t *buf, uint32_t size) {
  std::ofstream outfile(filepath.string(),
                        std::ios::out | std::ios::trunc | std::ios::binary);
  if (!outfile.is_open()) {
    return false;
  }

  outfile.write(reinterpret_cast<const char *>(buf), size);
  outfile.close();

  return true;
}

/*
 * Saves the original format of the selected items.
 */
bool VGMRoot::SaveAllAsRaw() {
  std::wstring dirpath = UI_GetSaveDirPath();
  if (dirpath.length() != 0) {
    for (auto file : vVGMFile) {
      bool result;
      std::experimental::filesystem::path filepath =
          dirpath +
          std::to_wstring(
              std::experimental::filesystem::path::preferred_separator) +
          *file->GetName();
      auto *buf =
          new uint8_t[file->unLength]; // Create a buffer the size of the file
      file->GetBytes(file->dwOffset, file->unLength, buf);
      result = UI_WriteBufferToFile(filepath, buf, file->unLength);
      delete[] buf;
    }
    return true;
  }

  return false;
}
