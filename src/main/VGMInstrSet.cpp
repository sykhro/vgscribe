#include <cstddef> // for size_t

#include "Root.h"    // for VGMRoot, pRoot
#include "VGMColl.h" // for VGMColl
#include "VGMInstrSet.h"
#include "VGMRgn.h"      // for VGMRgn
#include "VGMSampColl.h" // for VGMSampColl

#include <gsl/pointers>

class RawFile;

DECLARE_MENU(VGMInstrSet)

// ***********
// VGMInstrSet
// ***********

VGMInstrSet::VGMInstrSet(const std::string &format, /*FmtID fmtID,*/
                         RawFile *file, uint32_t offset, uint32_t length,
                         std::wstring name, VGMSampColl *theSampColl)
    : VGMFile(FILETYPE_INSTRSET, /*fmtID,*/ format, file, offset, length, name),
      sampColl(theSampColl) {
  AddContainer<VGMInstr>(aInstrs);
}

VGMInstrSet::~VGMInstrSet() {
  DeleteVect<VGMInstr>(aInstrs);
  delete sampColl;
}

VGMInstr *VGMInstrSet::AddInstr(uint32_t offset, uint32_t length, uint32_t bank,
                                uint32_t instrNum, const std::wstring &instrName) {
  std::wostringstream name;
  if (instrName.empty()) {
    name << L"Instrument " << aInstrs.size();
  } else {
    name << instrName;
  }

  aInstrs.emplace_back(
      new VGMInstr(this, offset, length, bank, instrNum, name.str()));
  return aInstrs.back();
}

bool VGMInstrSet::Load() {
  if (!GetHeaderInfo()) {
    return false;
  }
  if (!GetInstrPointers()) {
    return false;
  }
  if (!LoadInstrs()) {
    return false;
  }

  if (aInstrs.empty()) {
    return false;
  }

  if (unLength == 0) {
    SetGuessedLength();
  }


  if (!sampColl->Load()) {
      pRoot->AddLogItem(new LogItem(L"Failed to load VGMSampColl.",
                                    LOG_LEVEL_ERR, L"VGMInstrSet"));
   }

  LoadLocalData();
  UseLocalData();
  pRoot->AddVGMFile(this);
  return true;
}

bool VGMInstrSet::GetHeaderInfo() { return true; }

bool VGMInstrSet::GetInstrPointers() { return true; }

bool VGMInstrSet::LoadInstrs() {
  size_t nInstrs = aInstrs.size();
  for (size_t i = 0; i < nInstrs; i++) {
    if (!aInstrs[i]->LoadInstr()) {
      return false;
    }
  }
  return true;
}

bool VGMInstrSet::OnSaveAsDLS() {
  std::wstring filepath =
      pRoot->UI_GetSaveFilePath(ConvertToSafeFileName(name), L"dls");
  if (filepath.length() != 0) {
    return SaveAsDLS(filepath);
  }
  return false;
}

bool VGMInstrSet::OnSaveAsSF2() {
  std::wstring filepath =
      pRoot->UI_GetSaveFilePath(ConvertToSafeFileName(name), L"sf2");
  if (filepath.length() != 0) {
    return SaveAsSF2(filepath);
  }
  return false;
}

bool VGMInstrSet::SaveAsDLS(const std::wstring &filepath) {
  DLSFile dlsfile;
  bool dlsCreationSucceeded = false;

  if (!assocColls.empty()) {
    dlsCreationSucceeded = assocColls.front()->CreateDLSFile(dlsfile);
  } else {
    return false;
  }

  if (dlsCreationSucceeded) {
    return dlsfile.SaveDLSFile(filepath);
  }
  return false;
}

bool VGMInstrSet::SaveAsSF2(const std::wstring &filepath) {

  if (!assocColls.empty()) {
    gsl::not_null<SF2File *> sf2file = assocColls.front()->CreateSF2File();
    bool bResult = sf2file->SaveSF2File(filepath);

    delete sf2file;
    return bResult;
  }

  return false;

}

// ********
// VGMInstr
// ********

VGMInstr::VGMInstr(VGMInstrSet *instrSet, uint32_t offset, uint32_t length,
                   uint32_t theBank, uint32_t theInstrNum, const std::wstring &name)
    : VGMContainerItem(instrSet, offset, length, name), parInstrSet(instrSet),
      bank(theBank), instrNum(theInstrNum) {
  AddContainer<VGMRgn>(aRgns);
}

VGMInstr::~VGMInstr() { DeleteVect<VGMRgn>(aRgns); }

void VGMInstr::SetBank(uint32_t bankNum) { bank = bankNum; }

void VGMInstr::SetInstrNum(uint32_t theInstrNum) { instrNum = theInstrNum; }

VGMRgn *VGMInstr::AddRgn(VGMRgn *rgn) {
  aRgns.emplace_back(rgn);
  return rgn;
}

VGMRgn *VGMInstr::AddRgn(uint32_t offset, uint32_t length, int sampNum,
                         uint8_t keyLow, uint8_t keyHigh, uint8_t velLow,
                         uint8_t velHigh) {
  return aRgns.emplace_back(new VGMRgn(this, offset, length, keyLow, keyHigh, velLow,
                                velHigh, sampNum));
}

bool VGMInstr::LoadInstr() { return true; }
