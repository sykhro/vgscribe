#pragma once
#include <cstdint> // for uint32_t
#include <string>  // for wstring, string

#include "VGMFile.h" // for FileType, FileType::FILETYPE_MISC
#include "common.h"
//#include "Menu.h"

// ***********
// VGMMiscFile
// ***********

class RawFile;

class VGMMiscFile : public VGMFile {
public:
  VGMMiscFile(const std::string &format, RawFile *file, uint32_t offset,
              uint32_t length = 0, std::wstring name = L"VGMMiscFile");

  virtual FileType GetFileType() { return FILETYPE_MISC; }

  virtual bool LoadMain();
  bool Load() override;
};
