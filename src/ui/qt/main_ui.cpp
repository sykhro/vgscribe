#include "QtVGMRoot.h"     // for QtVGMRoot, qtVGMRoot
#include "mainwindow.h"    // for MainWindow
#include "qapplication.h"  // for QApplication, qApp
#include "qbytearray.h"    // for QByteArray
#include "qfile.h"         // for QFile
#include "qiodevice.h"     // for QIODevice::OpenModeFlag::ReadOnly
#include "qstring.h"       // for QLatin1String, QString

//! [main() function]
int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    qApp->setStyleSheet("QSplitter::handle{background-color: #B8B8B8;}");
//    QFile file(":/qss/default.qss");
    QFile file(":/qdarkstyle/style.qss");
    file.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(file.readAll());
    qApp->setStyleSheet(styleSheet);

    qtVGMRoot.Init();

    MainWindow window;
    window.resize(900, 600);
    window.show();
    return app.exec();
}
//! [main() function]

