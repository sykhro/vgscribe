#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <qlistview.h>
#include <qsplitter.h>
#include <qtreeview.h>
#include <QMainWindow>
#include <QMdiArea>

#include "VGMFileView.h"
#include "qmainwindow.h"  // for QMainWindow
#include "qobjectdefs.h"  // for Q_OBJECT
#include "qstring.h"      // for QString

class QDragEnterEvent;
class QDragMoveEvent;
class QDropEvent;
class QListView;
class QObject;
class QSplitter;
class QWidget;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

    static MainWindow& getInstance() {
        static MainWindow instance;
        return instance;
    }

protected:
    QSplitter *vertSplitter;
    QSplitter *horzSplitter;
    QSplitter *vertSplitterLeft;
    QListView *rawFileListView;
    QListView *vgmFileListView;
    QListView *vgmCollListView;
    QListView *collListView;

protected:
    void dragEnterEvent(QDragEnterEvent *event);
    void dragMoveEvent(QDragMoveEvent* event);
    void dropEvent(QDropEvent *event);
};

#endif // MAINWINDOW_H


