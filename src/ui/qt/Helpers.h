#ifndef VGMTRANS_HELPERS_H
#define VGMTRANS_HELPERS_H

#include <cstdint>  // for uint8_t
#include <QIcon>
#include <QColor>

#include "VGMFile.h"            // for FileType

QIcon iconForFileType(FileType filetype);
QColor colorForEventColor(uint8_t eventColor);
QColor textColorForEventColor(uint8_t eventColor);

#endif //VGMTRANS_HELPERS_H
