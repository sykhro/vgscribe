#include "VGMFileTreeView.h"

#include "qtreeview.h"  // for QTreeView

class QWidget;

VGMFileTreeView::VGMFileTreeView(VGMFile *file, QWidget *parent)
        : QTreeView(parent)
        , vgmfile(file)
        , model(file)
{
    this->setModel(&model);

}

VGMFileTreeView::~VGMFileTreeView()
{
}
