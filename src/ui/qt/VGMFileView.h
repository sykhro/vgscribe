#ifndef VGMTRANS_BREAKDOWNVIEW_H
#define VGMTRANS_BREAKDOWNVIEW_H

#include <qsplitter.h>    // for QSplitter
#include <QMainWindow>

#include "qobjectdefs.h"  // for Q_OBJECT
#include "qstring.h"      // for QString

class HexView;
class QObject;
class VGMFile;
class VGMFileTreeView;

class VGMFileView : public QSplitter {
    Q_OBJECT

public:
    VGMFileView(VGMFile *vgmFile);
    ~VGMFileView();

    void addToMdi();


protected:
    QSplitter *horzSplitter;
    HexView *hexView;
    VGMFileTreeView *treeView;
};


#endif //VGMTRANS_BREAKDOWNVIEW_H
