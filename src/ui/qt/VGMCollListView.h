//
// Created by Mike on 8/31/14.
//
#pragma once

#include <QAbstractListModel>
#include <QListView>
#include <QKeyEvent>               // for QKeyEvent
#include <QString>             // for QString
#include <QVariant>            // for QVariant

class VGMCollListViewModel : public QAbstractListModel
{
    Q_OBJECT

public:
    VGMCollListViewModel(QObject *parent = 0);

    int rowCount ( const QModelIndex & parent = QModelIndex() ) const;

    QVariant data ( const QModelIndex & index, int role = Qt::DisplayRole ) const;

public slots:
    void changedVGMColls();
};



class VGMCollListView : public QListView
{
public:
    VGMCollListView(QWidget *parent = 0);

    void keyPressEvent(QKeyEvent* e);
};
